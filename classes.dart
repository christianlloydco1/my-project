void main() {
  var person = new Person('Chris', 'O', 'Co');

  person.printName();
}

class Person {
  String firstName = '', middleInitial = '', surName = '';

  Person(this.firstName, this.middleInitial, this.surName);

  printName() {
    print(this.firstName + ' ' + this.middleInitial + '. ' + this.surName);
  }
}
