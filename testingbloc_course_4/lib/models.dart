import 'package:flutter/foundation.dart' show immutable;

// this class is to check is the user is logged in or not
@immutable
class LoginHandle {
  final String token;

  const LoginHandle({
    required this.token,
  });
  const LoginHandle.foobar() : token = 'foobar';

  //implement equality and hash code to compare two instances of login handle
  @override
  bool operator ==(covariant LoginHandle other) => token == other.token;

  @override
  int get hashCode => token.hashCode;

  @override
  String toString() => 'LoginHandle (token = $token)';
}

//enum class for error although there is only one error
enum LoginErrors { invalidHandle }

//note class that shows title of a note
@immutable
class Note {
  final String title;

  const Note({
    required this.title,
  });
  @override
  String toString() => 'Note (title = $title)';
}

//creating mocked notes
final mockNotes = Iterable.generate(
  3,
  (i) => Note(title: 'Note ${i + 1}'),
);
