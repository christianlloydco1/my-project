//add all strings that will be displayed at the application at string.dart
const emailOrPasswordEmptyDialogTitle =
    'Please fill in both email and password fields';

const emailOrPasswordEmptyDescription =
    'You seem to have forgotten to enter either the email or the password field, or both. Please complete both fields';

const loginErrorDialogTitle = 'Login Error';

const loginErrorDialogContent =
    'Invalid email/password combination. Please try again with valid login credentials!';

const pleaseWait = 'Please wait...';
const enterYourPasswordHere = 'Enter your password here...';
const enterYourEmailHere = 'Enter your email here...';
const ok = 'OK';
const login = 'Log in';
const homePage = 'Home Page';
