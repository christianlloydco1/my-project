import 'package:flutter/foundation.dart' show immutable;

//define actions or events that will be used in bloc
@immutable
abstract class AppAction {
  const AppAction();
}

//define login action
@immutable
class LoginAction implements AppAction {
  final String email;
  final String password;

  const LoginAction({
    required this.email,
    required this.password,
  });
}

@immutable
class LoadNotesAction implements AppAction {
  const LoadNotesAction();
}
