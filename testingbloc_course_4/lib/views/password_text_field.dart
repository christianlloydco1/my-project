import 'package:flutter/material.dart';
import 'package:testingbloc_course_4/strings.dart' show enterYourPasswordHere;

class PasswordextField extends StatelessWidget {
  final TextEditingController passwordController;
  const PasswordextField({
    Key? key,
    required this.passwordController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: passwordController,
      obscureText: true,
      obscuringCharacter: '⦿',
      autocorrect: false,
      decoration: const InputDecoration(
        hintText: enterYourPasswordHere,
      ),
    );
  }
}
