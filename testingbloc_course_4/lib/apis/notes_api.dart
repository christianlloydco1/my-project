import 'package:flutter/foundation.dart' show immutable;
import 'package:testingbloc_course_4/models.dart';

//to qualify as a NotesApiProtocol a class must define getNotes method that returns a future of optional iterable
@immutable
abstract class NotesApiProtocol {
  const NotesApiProtocol();
  Future<Iterable<Note>?> getNotes({
    required LoginHandle loginHandle,
  });
}

@immutable
class NotesApi implements NotesApiProtocol {
  @override
  Future<Iterable<Note>?> getNotes({
    required LoginHandle loginHandle,
  }) =>
      Future.delayed(
        const Duration(seconds: 2),
        //check if the loginHandle is equal to foobar if yes it will return mockNotes else return null
        () => loginHandle == const LoginHandle.foobar() ? mockNotes : null,
      );
}
