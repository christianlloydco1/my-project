import 'package:flutter/foundation.dart' show immutable;
import '../models.dart';

//to qualify as a LoginApiProtocol a class must define a login method taht has email and password fields
@immutable
abstract class LoginApiProtocol {
  const LoginApiProtocol();
  Future<LoginHandle?> login({
    required String email,
    required String password,
  });
}

//implements LoginApiProtocol
@immutable
class LoginApi implements LoginApiProtocol {
  @override
  Future<LoginHandle?> login({
    required String email,
    required String password,
  }) =>
      Future.delayed(
        const Duration(seconds: 2),
        //creating future boolean if email is foo@bar.com and password is foobar boolean value will be true
        () => email == 'foo@bar.com' && password == 'foobar',
        //create login handle foobar if the future boolean is true
      ).then((isLoggedIn) => isLoggedIn ? const LoginHandle.foobar() : null);
}
