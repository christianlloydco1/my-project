import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testingbloc_course_6/bloc/app_bloc.dart';
import 'package:testingbloc_course_6/bloc/app_state.dart';
import 'package:testingbloc_course_6/bloc/bloc_events.dart';
import 'package:testingbloc_course_6/extension/stream/start_with.dart';

//allows us to create two instance of app bloc, one will work with the top bloc and one with the bottom bloc
class AppBlocView<T extends AppBloc> extends StatelessWidget {
  const AppBlocView({Key? key}) : super(key: key);

  //create periodic stream that has startWith and upon this stream firing an event we're gonna go to that particular app provided by T generic parameter and send an event to it and say to load the next image
  void startUpdatingBloc(BuildContext context) {
    Stream.periodic(
      const Duration(seconds: 10),
      (_) => const LoadNextUrlEvent(),
    ).startWith(const LoadNextUrlEvent()).forEach((event) {
      context.read<T>().add(event);
    });
  }

  @override
  Widget build(BuildContext context) {
    startUpdatingBloc(context);
    return Expanded(
      child: BlocBuilder<T, AppState>(
        builder: (context, appState) {
          if (appState.error != null) {
            return const Text('An error occured. Try again in a moment!');
          } else if (appState.data != null) {
            return Image.memory(
              appState.data!,
              fit: BoxFit.fitHeight,
            );
          } else {
            //loading
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
