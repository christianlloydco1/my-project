import 'package:async/async.dart' show StreamGroup;

//created to start fetching image immediately instead of waiting 10 seconds before fetching an image
extension StartWith<T> on Stream<T> {
  Stream<T> startWith(T value) => StreamGroup.merge([
        this,
        Stream<T>.value(value),
      ]);
}
