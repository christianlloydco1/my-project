import '../bloc/person.dart';
// for the bloc to accept different event
import 'package:flutter/foundation.dart' show immutable;

const persons1url = 'http://192.168.100.128:5500/api/persons1.json';
const persons2url = 'http://192.168.100.128:5500/api/persons2.json';
//type definition of a function that takes url and returns a future of iterable of persons
typedef PersonsLoader = Future<Iterable<Person>> Function(String url);

@immutable
abstract class LoadAction {
  const LoadAction();
}

// define action for laoding persons, we add persons loader so that we can inject any loader in this class adn mock it
//Adding Persons loader will enable us to to create loader that is independent on url you send
@immutable
class LoadPersonsAction implements LoadAction {
  final String url;
  final PersonsLoader loader;
  const LoadPersonsAction({required this.url, required this.loader}) : super();
}
