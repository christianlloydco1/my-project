import 'package:flutter/foundation.dart' show immutable;
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/person.dart';
import '../bloc/bloc_actions.dart';

//compare iterable independent of their ordering because iterable may return same object with different ordering
//compare length of intersection
extension IsEqualIgnoringOrdering<T> on Iterable<T> {
  bool isEqualIgnoringOrdering(Iterable<T> other) =>
      length == other.length &&
      {...this}.intersection({...other}).length == length;
}

//define the result of the bloc
@immutable
class FetchResult {
  final Iterable<Person> persons;
  final bool isRetrievedFromCache;

  const FetchResult({
    required this.persons,
    required this.isRetrievedFromCache,
  });

  @override
  String toString() =>
      'FetchResult (isRetrievedFromCache = $isRetrievedFromCache, persons = $persons)';
  //add equality operator to see if two instance of fetch result is equal.
  @override
  bool operator ==(covariant FetchResult other) =>
      persons.isEqualIgnoringOrdering(other.persons) &&
      isRetrievedFromCache == other.isRetrievedFromCache;

  @override
  int get hashCode => Object.hash(persons, isRetrievedFromCache);
}

//bloc header
class PersonsBloc extends Bloc<LoadAction, FetchResult?> {
  final Map<String, Iterable<Person>> _cache = {};
  PersonsBloc() : super(null) {
    //Handling event
    on<LoadPersonsAction>(
      (event, emit) async {
        //check if the url already exist in the map _cache and emit it
        final url = event.url;
        if (_cache.containsKey(url)) {
          final cachePersons = _cache[url]!;
          final result = FetchResult(
            persons: cachePersons,
            isRetrievedFromCache: true,
          );
          emit(result);
        } else {
          final loader = event.loader;
          //If the value is not yet on cache we will use getPersons method and save it to _cache
          final persons = await loader(url);
          _cache[url] = persons;
          final result = FetchResult(
            persons: persons,
            isRetrievedFromCache: false,
          );
          //telling to whatever is listening to the bloc that this is the event that they will consume
          emit(result);
        }
      },
    );
  }
}
