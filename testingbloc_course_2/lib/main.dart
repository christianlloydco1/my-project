import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

import 'dart:developer' as devtools show log;

extension Log on Object {
  void log() => devtools.log(toString());
}

void main() {
  runApp(
    MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (_) => PersonsBloc(),
        child: const HomePage(),
      ),
    ),
  );
}

@immutable
abstract class LoadAction {
  const LoadAction();
}

@immutable
class LoadPersonsAction implements LoadAction {
  final PersonUrl url;
  const LoadPersonsAction({required this.url}) : super();
}

enum PersonUrl {
  persons1,
  persons2,
}

// Add a subscript extension to Iterable<T>
const Iterable<String> names = ['foo', 'bar'];
void testit() {
  final baz = names[2];
}

extension Subscript<T> on Iterable<T> {
  T? operator [](int index) => length > index ? elementAt(index) : null;
}

extension UrlString on PersonUrl {
  String get urlString {
    switch (this) {
      case PersonUrl.persons1:
        return 'http://192.168.100.128:5500/api/persons1.json';
      case PersonUrl.persons2:
        return 'http://192.168.100.128:5500/api/persons2.json';
    }
  }
}

@immutable
class Person {
  final String name;
  final int age;

  Person({
    required this.name,
    required this.age,
  });

  Person.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String,
        age = json['age'];
  //named constructor

  @override
  String toString() => 'Person (name = $name, age =$age)';
}

//downlaod and parse json .then syntax

Future<Iterable<Person>> getPersons(String url) => HttpClient()
    .getUrl(Uri.parse(url)) //makes request
    .then((req) => req.close()) //close the request
    .then((resp) =>
        resp.transform(utf8.decoder).join()) //transform the request into string
    .then((str) => json.decode(str) as List<dynamic>) //Make the string a list
    .then((list) => list.map(
        (e) => Person.fromJson(e))); //List becomes iterable of Person class

//define the result of the bloc
@immutable
class FetchResult {
  final Iterable<Person> persons;
  final bool isRetrievedFromCache;

  const FetchResult({
    required this.persons,
    required this.isRetrievedFromCache,
  });

  @override
  String toString() =>
      'FetchResult (isRetrievedFromCache = $isRetrievedFromCache, persons = $persons)';
}

//bloc header
class PersonsBloc extends Bloc<LoadAction, FetchResult?> {
  final Map<PersonUrl, Iterable<Person>> _cache = {};
  //Handle the LoadPersonsAction in the constructor
  PersonsBloc() : super(null) {
    on<LoadPersonsAction>(
      (event, emit) async {
        //event is the input on the bloc and emit is the output
        final url = event.url;
        if (_cache.containsKey(url)) {
          final cachePersons = _cache[url]!;
          final result = FetchResult(
            persons: cachePersons,
            isRetrievedFromCache: true,
          );
          emit(result);
        } else {
          final persons = await getPersons(url.urlString);
          _cache[url] = persons;
          final result = FetchResult(
            persons: persons,
            isRetrievedFromCache: false,
          );
          emit(result);
        }
      },
    );
  }

  //cache in bloc

}

//Define the result of bloc

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //add row with two buttons and actions to column
      body: Column(
        children: [
          Row(
            children: [
              TextButton(
                onPressed: () {
                  context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                          url: PersonUrl.persons1,
                        ),
                      ); // like of function goes up in the widget tree and find the provider that gives access to the bloc.
                },
                child: const Text('Load json#1'),
              ),
              TextButton(
                onPressed: () {
                  context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                          url: PersonUrl.persons2,
                        ),
                      );
                },
                child: const Text('Load json#2'),
              ),
            ],
          ),
          BlocBuilder<PersonsBloc, FetchResult?>(
            buildWhen: (previousResult, currentResult) {
              return previousResult?.persons != currentResult?.persons;
            },
            builder: ((context, fetchResult) {
              fetchResult?.log();
              final persons = fetchResult?.persons;
              if (persons == null) {
                return const SizedBox();
              }
              return Expanded(
                child: ListView.builder(
                  itemCount: persons.length,
                  itemBuilder: (context, index) {
                    final person = persons[index]!;
                    return ListTile(
                      title: Text('Name: ${person.name} \nAge: ${person.age}'),
                    );
                  },
                ),
              );
            }),
          )
        ],
      ),
      appBar: AppBar(
        title: const Text('Put your title'),
      ),
    );
  }
}
