// @dart=2.9
import 'package:crud/models/data_model.dart';
import 'package:crud/utils/db_helper.dart';

class DBService {
  Future<bool> addStudent(StudentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(StudentModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> registerStudent(StudentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(StudentModel.registrationTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> studentSubjects(SubjectModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insertSubject(SubjectModel.subjectTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateStudent(StudentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.update(StudentModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateSubject(SubjectModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateSubject(SubjectModel.subjectTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<List<StudentModel>> getStudents() async {
    await DB.init();
    List<Map<String, dynamic>> students = await DB.query(StudentModel.table);

    return students.map((item) => StudentModel.fromMap(item)).toList();
  }

  Future<List<SubjectModel>> getSubjects(int id) async {
    await DB.init();
    List<Map<String, dynamic>> subjects =
        await DB.querySubjects(SubjectModel.subjectTable, id);

    return subjects.map((item) => SubjectModel.fromMap(item)).toList();
  }

  Future<List<StudentModel>> getPendingStudents() async {
    await DB.init();
    List<Map<String, dynamic>> students =
        await DB.query(StudentModel.registrationTable);

    return students.map((item) => StudentModel.fromMap(item)).toList();
  }

  Future<List<CategoryModel>> getCategories() async {
    await DB.init();
    List<Map<String, dynamic>> categories = await DB.query(CategoryModel.table);

    return categories.map((item) => CategoryModel.fromMap(item)).toList();
  }

  Future<bool> deleteStudent(StudentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(StudentModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> deleteSubject(SubjectModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.deleteSubject(SubjectModel.subjectTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> deleteStudentRegistration(StudentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(StudentModel.registrationTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  // Future<StudentModel> getLoginUser(String email, String password) async {
  //   var dbClient = await _db;
  //   var res = await dbClient.rawQuery("SELECT * FROM students WHERE "
  //       "email = '$email' AND "
  //       "password = '$password'");

  //   if (res.length > 0) {
  //     return StudentModel.fromMap(res.first);
  //   }

  //   return null;
  // }
}
