// @dart=2.9
import 'dart:async';
import 'package:crud/models/model.dart';
import 'package:path/path.dart' as p;
import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';

import '../models/data_model.dart';

class DB {
  static Database _db;

  static int get _version => 1;

  static Future<void> init() async {
    if (_db != null) {
      return;
    }

    try {
      var databasesPath = await getDatabasesPath();
      String _path = p.join(databasesPath, 'crud2.db');
      _db = await openDatabase(_path, version: _version, onCreate: onCreate);
    } catch (ex) {
      print(ex);
    }
  }

  Future<StudentModel> checkLogin(String email, String password) async {
    var dbClient = await _db;
    var res = await dbClient.rawQuery("SELECT * FROM students WHERE "
        "email = '$email' AND "
        "password = '$password'");

    if (res.length > 0) {
      return StudentModel.fromMap(res.first);
    }

    return null;
  }

  // Future<StudentModel> checkLogin(String email, String password) async {
  //   final dbClient = await _db;
  //   var res = await dbClient.rawQuery(
  //       "SELECT * FROM students WHERE email = '$email' and password = '$password'");

  //   if (res.length > 0) {
  //     return StudentModel.fromMap(res.first);
  //   }

  //   return null;
  // }

  static void onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE registration (id INTEGER PRIMARY KEY AUTOINCREMENT, email STRING, password STRING, studentName STRING, sectionId INTEGER, address STRING, bDay STRING, contact TEXT NOT NULL UNIQUE, gender STRING, studentPic STRING)');
    await db.execute(
        'CREATE TABLE students (id INTEGER PRIMARY KEY AUTOINCREMENT, email STRING, password STRING, studentName STRING, sectionId INTEGER, address STRING, bDay STRING, contact TEXT NOT NULL UNIQUE, gender STRING, studentPic STRING)');
    await db.execute(
        'CREATE TABLE section_categories (id INTEGER PRIMARY KEY AUTOINCREMENT, categoryName STRING)');
    await db.execute(
        "INSERT INTO section_categories (categoryName) VALUES ('BSIT 4-1'), ('BSIT 4-2'), ('BSIT 4-3'),  ('BSIT 4-4');");
    await db.execute(
        'CREATE TABLE subjects (subjectId INTEGER PRIMARY KEY AUTOINCREMENT, subjectName TEXT, roomNum TEXT, professor TEXT, schedule TEXT, subjectCode TEXT, grade REAL, id INTEGER, FOREIGN KEY (id) REFERENCES students(id))');
  }

  static Future<List<Map<String, dynamic>>> query(String table) async =>
      _db.query(table);

  static Future<List<Map<String, dynamic>>> querySubjects(
          String table, int id) async =>
      _db.query(
        table,
        where: 'id = ?',
        whereArgs: [id],
      );

  // static Future<List<Map<String, dynamic>>> queryStudent(String table, StudentModel model) async =>
  //     _db.query(table,
  //     where: 'email = ?', whereArgs: model.email,);

  static Future<int> insert(String table, Model model) async =>
      await _db.insert(table, model.toMap());

  static Future<int> insertSubject(String table, SubjectModel model) async {
    for (int x = 0; x < model.subjectNames.length; x++) {
      model.subjectName = model.subjectNames[x];
      if (model.subjectName == 'Science') {
        model.subjectCode = "GEED 1";
        model.roomNum = "S-101";
        model.professor = "Mr. Juan Dela Cruz";
        model.schedule = "M T 7:00 AM - 12:00 NN";
      } else if (model.subjectName == 'Physics') {
        model.subjectCode = "GEED 2";
        model.roomNum = "E-202";
        model.professor = "Mrs. Alexandra Bonifacio";
        model.schedule = "M T 1:00 AM - 5:00 PM";
      } else if (model.subjectName == 'General Calculus') {
        model.subjectCode = "GEED 3";
        model.roomNum = "W-405";
        model.professor = "Mr. Edward Mores";
        model.schedule = "W TH 7:00 AM - 12:00 NN";
      } else if (model.subjectName == 'Computer Programming') {
        model.subjectCode = "GEED 4";
        model.roomNum = "S-206";
        model.professor = "Mrs. Andrea Fabregas";
        model.schedule = "W TH 12:00 AM - 5:00 PM";
      } else if (model.subjectName == 'Applications Development') {
        model.subjectCode = "GEED 5";
        model.roomNum = "W-206";
        model.professor = "Mr. Pedro Ocampo";
        model.schedule = "F 5:00 AM - 12:00 NN";
      }
      await _db.insert(table, model.toMap());
    }
  }

  static Future<int> register(String table, Model model) async =>
      await _db.insert(table, model.toMap());

  static Future<int> update(String table, Model model) async => await _db
      .update(table, model.toMap(), where: 'id = ?', whereArgs: [model.id]);

  static Future<int> updateSubject(String table, SubjectModel model) async =>
      await _db.update(table, model.toMap(),
          where: 'id = ? AND subjectName = ?',
          whereArgs: [model.studentId, model.subjectName]);

  static Future<int> delete(String table, Model model) async =>
      await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);

  static Future<int> deleteSubject(String table, SubjectModel model) async =>
      await _db.delete(table, where: 'subjectId = ?', whereArgs: [model.id]);

  static Future<Batch> batch() async => _db.batch();

  Future<StudentModel> getLoginUser(String email, String password) async {
    await DB.init();
    var dbClient = await _db;

    var res = await dbClient.rawQuery("SELECT * FROM students WHERE "
        "email = '$email' AND "
        "password = '$password'");

    if (res.length > 0) {
      return StudentModel.fromMap(res.first);
    }

    return null;
  }

  Future<StudentModel> getSpecificUser(String email, String password) async {
    await DB.init();
    var dbClient = await _db;

    var res = await dbClient.rawQuery("SELECT * FROM students WHERE "
        "email = '$email' AND "
        "password = '$password'");

    if (res.length > 0) {
      return StudentModel.fromMap(res.first);
    }

    return null;
  }

  Future<int> updateUser(StudentModel user) async {
    await DB.init();
    var dbClient = await _db;
    var res = await dbClient.update('students', user.toMap(),
        where: 'id = ?', whereArgs: [user.id]);
    return res;
  }

  // Future<StudentModel> getLoginUser(String email, String password) async {
  //   var dbClient = await _db;
  //   var res = await dbClient.rawQuery(
  //       "SELECT * FROM students WHERE email = '$email' AND password = '$password'");

  //   if (res.length > 0) {
  //     return StudentModel.fromMap(res.first);
  //   }
  //   return null;
  // }

//   static Future<List<Map<String, dynamic>>> checkLogin(String table, Model model) async =>
//       _db.rawQuery(
//         "SELECT * FROM students WHERE username = '${model.email}' and password = '${model.password}'");

// Future<StudentModel> checkLogin(String userName, String password) async {
//     await DB.init();
//     final dbClient = await _db;
//     var res = await dbClient.rawQuery(
//         "SELECT * FROM students WHERE username = '$userName' and password = '$password'");

//     if (res.length > 0) {
//       return new StudentModel.fromMap();
//     }

//     return null;

//   }

}
