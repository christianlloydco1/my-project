// @dart=2.9
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:image_picker/image_picker.dart';

class FormHelper {
  static Widget textInputEmail(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: 'chris@gmail.com',
        labelText: 'Email',
        fillColor: Colors.white,
        focusColor: Colors.green,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputGrade(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return SizedBox(
      height: 50,
      width: 100,
      child: TextFormField(
        style: TextStyle(
            fontFamily: 'Poppins', fontWeight: FontWeight.bold, fontSize: 20),
        textAlign: TextAlign.center,
        initialValue: initialValue != "null" ? initialValue.toString() : "",
        decoration: InputDecoration(
          hintText: '',
          fillColor: Colors.white,
          focusColor: Colors.green,
          contentPadding: EdgeInsets.only(top: 15),
          filled: true,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
        onChanged: (String value) {
          return onChanged(value);
        },
        validator: (value) {
          return onValidate(value);
        },
      ),
    );
  }

  static Widget textInputPassword(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        labelText: 'Password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        fillColor: Colors.white,
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputName(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: 'Juan Dela Cruz',
        labelText: 'Name',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputAddress(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: 'E.R. Ph 2 Blk 48 Lot 9, San Isidro, Rodriguez, Rizal',
        labelText: 'Address',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputBday(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: 'MM/DD/YYYY',
        labelText: 'Birthday',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  // DateTimePicker(
  //                     controller: _birthdayController,
  //                     initialDate: new DateTime(2000, 1, 1),
  //                     firstDate: DateTime(1900),
  //                     lastDate: DateTime(2022),
  //                     onChanged: bloc.changeBirthday,
  //                     decoration: InputDecoration(
  //                       hintText: 'MM/DD/YYYY',
  //                       labelText: 'Birthday',
  //                       errorText: snapshot.error?.toString(),
  //                     ),
  //                     validator: (val) {
  //                       print(val);
  //                       return null;
  //                     },
  //                     onSaved: (val) => print(val),
  //                   );
  static Widget textInputContact(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: '095456987456',
        labelText: 'Contact Number',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputGender(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        hintText: 'Male/Female',
        labelText: 'Gender',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget selectDropdown(
    BuildContext context,
    Object initialValue,
    dynamic data,
    Function onChanged, {
    Function onValidate,
  }) {
    return Container(
      height: 90,
      padding: EdgeInsets.only(top: 5),
      child: SizedBox(
        child: new DropdownButtonFormField<String>(
          decoration: InputDecoration(
            hintText: 'Select Section',
            labelText: 'Section',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          isExpanded: true,
          hint: new Text("Select section"),
          value: initialValue != null ? initialValue.toString() : null,
          isDense: true,
          onChanged: (String newValue) {
            FocusScope.of(context).requestFocus(new FocusNode());
            onChanged(newValue);
          },
          validator: (value) {
            return onValidate(value);
          },
          items: data.map<DropdownMenuItem<String>>(
            (CategoryModel data) {
              return DropdownMenuItem<String>(
                value: data.categoryId.toString(),
                child: new Text(
                  data.categoryName,
                  style: new TextStyle(color: Colors.black),
                ),
              );
            },
          ).toList(),
        ),
      ),
    );
  }

  static Widget fieldLabel(String labelName) {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
      child: Text(
        labelName,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15.0,
        ),
      ),
    );
  }

  static Widget picPicker(String fileName, Function onFilePicked) {
    Future<PickedFile> _imageFile;
    ImagePicker _picker = new ImagePicker();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.image, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.gallery);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                icon: Icon(Icons.camera, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.camera);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
          ],
        ),
        fileName != null
            ? Container(
                decoration: BoxDecoration(
                  color: Colors.green,
                  border: Border.all(
                    color: Colors.grey[500],
                  ),
                ),
                width: 200,
                height: 200,
                child: Image.file(
                  File(fileName),
                  fit: BoxFit.fill,
                ),
              )
            : Container(
                decoration: BoxDecoration(
                  color: Colors.green,
                  border: Border.all(
                    color: Colors.grey[500],
                  ),
                ),
                padding: EdgeInsets.only(left: 5, top: 70),
                width: 200,
                height: 200,
                child: Column(
                  children: [
                    Text(
                      'Select Photo',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_a_photo,
                      color: Colors.white,
                    )
                  ],
                ),
              )
      ],
    );
  }

  static void showMessage(BuildContext context, String title, String message,
      String buttonText, Function onPressed,
      {bool isConfirmationDialog = false,
      String buttonText2 = "",
      Function onPressed2}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: [
            new FlatButton(
              onPressed: () {
                return onPressed();
              },
              child: new Text(buttonText),
            ),
            Visibility(
              visible: isConfirmationDialog,
              child: new FlatButton(
                onPressed: () {
                  return onPressed2();
                },
                child: new Text(buttonText2),
              ),
            ),
          ],
        );
      },
    );
  }
}
