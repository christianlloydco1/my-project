import 'package:crud/models/model.dart';
import 'package:flutter/src/material/data_table.dart';

class StudentModel extends Model {
  static String registrationTable = 'registration';
  static String table = 'students';

  int id;
  String email;
  String password;
  String studentName;
  int sectionId;
  String address;
  String bDay;
  String contact;
  String gender;
  String studentPic;

  StudentModel({
    required this.id,
    required this.email,
    required this.password,
    required this.studentName,
    required this.sectionId,
    required this.address,
    required this.bDay,
    required this.contact,
    required this.gender,
    required this.studentPic,
  });
  //fetching
  static StudentModel fromMap(Map<String, dynamic> map) {
    return StudentModel(
      email: map["email"],
      password: map["password"],
      id: map["id"],
      studentName: map['studentName'].toString(),
      sectionId: map['sectionId'],
      address: map['address'],
      bDay: map['bDay'],
      contact: map['contact'].toString(),
      gender: map['gender'],
      studentPic: map['studentPic'],
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': id,
      'email': email,
      'password': password,
      'studentName': studentName,
      'sectionId': sectionId,
      'address': address,
      'bDay': bDay,
      'contact': contact,
      'gender': gender,
      'studentPic': studentPic,
    };

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class SubjectModel extends Model {
  static String subjectTable = 'subjects';

  int id;
  List<dynamic> subjectNames;
  String subjectName;
  String subjectCode;
  String roomNum;
  String professor;
  String schedule;
  String grade;
  int studentId;

  SubjectModel({
    required this.id,
    required this.subjectNames,
    required this.subjectName,
    required this.subjectCode,
    required this.roomNum,
    required this.professor,
    required this.schedule,
    required this.grade,
    required this.studentId,
  });
  //fetching
  static SubjectModel fromMap(Map<String, dynamic> map) {
    return SubjectModel(
      id: map["subjectId"],
      subjectNames: [],
      subjectName: map["subjectName"],
      subjectCode: map["subjectCode"],
      roomNum: map["roomNum"],
      professor: map["professor"],
      schedule: map["schedule"],
      grade: map["grade"].toString(),
      studentId: map["id"],
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'subjectId': id,
      'subjectName': subjectName,
      'subjectCode': subjectCode,
      'roomNum': roomNum,
      'professor': professor,
      'schedule': schedule,
      'grade': grade,
      'id': studentId,
    };

    if (id != null) {
      map['subjectId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class CategoryModel extends Model {
  static String table = 'section_categories';

  String categoryName;
  int categoryId;

  CategoryModel({
    required this.categoryId,
    required this.categoryName,
  });

  static CategoryModel fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      categoryId: map["id"],
      categoryName: map['categoryName'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'categoryName': categoryName,
    };

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }
}
