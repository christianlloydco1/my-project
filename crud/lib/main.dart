// @dart=2.9

import 'package:crud/pages/Admin/give_grades.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/Students/enrollment_page.dart';

import 'package:crud/pages/Students/student_home.dart';
import 'package:crud/pages/login_page.dart';
import 'package:crud/pages/on_boarding.dart';

import 'package:crud/pages/Students/student_register.dart';
import 'package:flutter/material.dart';
import 'pages/Admin/pending_registration.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQFLITE CRUD',
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
