// @dart=2.9
import 'package:crud/Comm/comHelper.dart';
import 'package:crud/pages/Admin/add_student_grade.dart';
import 'package:crud/pages/Admin/give_grades.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class EditStudentGrade extends StatefulWidget {
  EditStudentGrade(
      {Key key, this.subjectModel, this.model, this.isEditMode = true})
      : super(key: key);
  StudentModel model;
  SubjectModel subjectModel;
  bool isEditMode;

  @override
  _EditStudentGradeState createState() => _EditStudentGradeState();
}

class _EditStudentGradeState extends State<EditStudentGrade> {
  SubjectModel subjectModel;
  StudentModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    subjectModel = new SubjectModel();
    model = new StudentModel();

    if (widget.isEditMode) {
      subjectModel = widget.subjectModel;
      model = widget.model;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green,
        automaticallyImplyLeading: true,
        title: Text("Student Grade"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Student   :   ${model.studentName}',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'Professor :   ${subjectModel.professor}',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 150,
                ),
                Column(
                  children: [
                    Text(
                      '${subjectModel.subjectName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      '${subjectModel.subjectCode}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FormHelper.textInputGrade(
                      context,
                      subjectModel.grade,
                      (value) => {
                        subjectModel.grade = value,
                      },
                      onValidate: (value) {
                        String pattern = r'^[1-9]\d*(\.\d+)?$';
                        RegExp regExp = new RegExp(pattern);
                        if (value.toString().isEmpty) {
                          return 'Please enter grade';
                        } else if (value.toString == "") {
                          return 'Please enter grade';
                        } else if (!regExp.hasMatch(value)) {
                          print('value : ${value.toString()}');
                          return 'Grade should range from 1.00 - 5.00';
                        } else if (double.parse(value) < 1.0 ||
                            double.parse(value) > 5.0) {
                          print(' value : ${value.toString()}');
                          return 'Grade should range from 1.00 - 5.00';
                        }

                        return null;
                      },
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    btnSubmit(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.green,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            String pattern = r'^[1-9]\d*(\.\d+)?$';
            RegExp regExp = new RegExp(pattern);
            if (subjectModel.grade.isEmpty) {
              alertDialog(context, 'Please Input Grade');
            } else if (subjectModel.grade == "null") {
              alertDialog(context, 'Please Input Grade');
            } else if (!regExp.hasMatch(subjectModel.grade)) {
              alertDialog(context, 'Grade should range from 1.00 - 5.00');
            } else if (double.parse(subjectModel.grade) < 1.0 ||
                double.parse(subjectModel.grade) > 5.0) {
              alertDialog(context, 'Grade should range from 1.00 - 5.00');
            } else {
              print(subjectModel.toMap());
              print(subjectModel.grade);

              dbService.updateSubject(subjectModel).then((value) {
                FormHelper.showMessage(
                  context,
                  "Update Student Record",
                  "Student record updated successfully",
                  "Ok",
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GiveGradesPage(),
                      ),
                    );
                  },
                );
              });
            }
          },
          child: subjectModel.grade != "null"
              ? Text('Edit Grade')
              : Text('Give Grade')),
    );
  }
}
