// @dart=2.9

import 'dart:io';
import 'package:crud/pages/Admin/add_student_grade.dart';
import 'package:crud/pages/Admin/give_grades.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/add_student.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

import 'edit_student.dart';

class GiveGradesPage extends StatefulWidget {
  @override
  _GiveGradesPageState createState() => _GiveGradesPageState();
  GiveGradesPage(
      {Key key, this.model, this.isEditMode = true, SubjectModel subjectModel})
      : super(key: key);
  StudentModel model;
  SubjectModel subjectModel;
  bool isEditMode;
}

class _GiveGradesPageState extends State<GiveGradesPage> {
  DBService dbService;

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Grades',
        ),
        backgroundColor: Colors.green,
      ),
      body: _fetchData(),
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 143, 236, 146),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 220,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.green,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: const Text(
                        'AN',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      'Admin Name',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'admin@gmail.com',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.groups,
              ),
              title: const Text(
                'Students Information',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => StudentsInformationPage()),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.hourglass_top,
              ),
              title: const Text(
                'Pending Registration',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => PendingRegistrationPage()),
                );
              },
            ),
            ListTile(
              leading: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Icon(
                  Icons.text_increase_outlined,
                  color: Colors.green,
                ),
              ),
              title: const Text(
                'Grades',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              tileColor: Colors.green,
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => GiveGradesPage()),
                );
              },
            ),
            SizedBox(
              height: 310,
            ),
            ListTile(
              leading: Icon(
                Icons.logout_sharp,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.green,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/givegrade.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<StudentModel>>(
              future: dbService.getStudents(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<StudentModel>> students) {
                if (students.hasData) {
                  return _buildUI(students.data);
                }

                return CircularProgressIndicator();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<StudentModel> students) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(students)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(10),
    );
  }

  Widget _buildDataTable(List<StudentModel> model) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text(
            "ID",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            "Name",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            "      Actions",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ],
      sortColumnIndex: 1,
      rows: model
          .map(
            (data) => DataRow(
              cells: <DataCell>[
                DataCell(
                  Text(
                    '${data.id}',
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                DataCell(
                  Text(
                    '${data.studentName}',
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
                DataCell(
                  Center(
                    child: new IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Container(child: Icon(Icons.text_increase)),
                      color: Colors.green,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AddGradePage(
                              isEditMode: true,
                              model: data,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
          .toList(),
    );
  }
}
