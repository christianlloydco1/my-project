// @dart=2.9
import 'package:crud/Comm/genTextFormField.dart';
import 'package:crud/pages/Admin/edit_student.dart';
import 'package:crud/pages/Admin/edit_student_grade.dart';
import 'package:crud/pages/Admin/give_grades.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/Students/enrollment_page.dart';
import 'package:crud/pages/Students/student_home.dart';
import 'package:crud/pages/Students/subject_page.dart';
import 'package:crud/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class AddGradePage extends StatefulWidget {
  AddGradePage(
      {Key key, this.model, this.isEditMode = true, SubjectModel subjectModel})
      : super(key: key);
  StudentModel model;
  SubjectModel subjectModel;
  bool isEditMode;

  @override
  _AddGradePageState createState() => _AddGradePageState();
}

extension Ext on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

class _AddGradePageState extends State<AddGradePage> {
  StudentModel model;
  SubjectModel subjectModel;
  // SubjectModel subjectModel;

  DBService dbService;
  bool science = false;
  bool physics = false;
  bool genCal = false;
  bool java = false;
  bool appDev = false;
  // bool value = false;
  List data = [
    'Science',
    'Physics',
    'Calculus',
    'Computer Programming',
    'Applications Development'
  ];
  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new StudentModel();
    subjectModel = new SubjectModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green,
        automaticallyImplyLeading: true,
        title: Text("Subjects"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    subjectModel.studentId = model.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.green,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/subjects.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<SubjectModel>>(
              future: dbService.getSubjects(model.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<SubjectModel>> subjects) {
                if (subjects.hasData) {
                  return _buildUI(subjects.data);
                }

                return Text('No Subjects Enrolled');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<SubjectModel> subjects) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(subjects)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(List<SubjectModel> subjectModel) {
    double grade;

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Subject\nName",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Subject\nCode",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Professor",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Grade",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Action",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: subjectModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.subjectName}',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.subjectCode}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),

                  DataCell(
                    Text(
                      '${data.professor}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),

                  DataCell(
                    data.grade == "null"
                        ? Text(
                            ' ',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          )
                        : Text(
                            '${data.grade}',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                  ),

                  DataCell(ElevatedButton(
                    child: Text('Grade'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      onPrimary: Colors.white,
                    ),
                    onPressed: () {
                      print(data.grade);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EditStudentGrade(
                            isEditMode: true,
                            subjectModel: data,
                            model: model,
                          ),
                        ),
                      );
                    },
                  )),

                  // DataCell(
                  //   Center(
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //       children: <Widget>[
                  //         new IconButton(
                  //           padding: EdgeInsets.all(0),
                  //           icon: Icon(Icons.delete),
                  //           color: Colors.red,
                  //           onPressed: () {
                  //             FormHelper.showMessage(
                  //               context,
                  //               "SQFLITE CRUD",
                  //               "Do you want to delete this record?",
                  //               "Yes",
                  //               () {
                  //                 dbService.deleteSubject(data).then((value) {
                  //                   setState(() {
                  //                     Navigator.of(context).pop();
                  //                   });
                  //                 });
                  //               },
                  //               buttonText2: "No",
                  //               isConfirmationDialog: true,
                  //               onPressed2: () {
                  //                 Navigator.of(context).pop();
                  //               },
                  //             );
                  //           },
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.green,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          subjectModel.studentId = model.id;
          subjectModel.subjectNames = selectedData;
          dbService.studentSubjects(subjectModel);

          FormHelper.showMessage(
            context,
            "Add Subject/s",
            "${selectedData.length} subject/s successfully enrolled.",
            "Ok",
            () {
              Navigator.pop(context, true);
            },
          );
        },

        // {
        //   if (validateAndSave()) {
        //     print(model.toMap());

        //     dbService.updateStudent(model).then((value) {
        //       FormHelper.showMessage(
        //         context,
        //         "Update Student Record",
        //         "Student record updated successfully",
        //         "Ok",
        //         () {
        //           Navigator.pop(context, true);
        //         },
        //       );
        //     });
        //   }
        // },
        child: Text('Add Subject'),
      ),
    );
  }
}
