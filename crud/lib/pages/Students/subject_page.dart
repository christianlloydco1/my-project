// @dart=2.9
import 'package:crud/pages/Admin/edit_student.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/Students/enrollment_page.dart';
import 'package:crud/pages/Students/student_home.dart';
import 'package:crud/pages/Students/subject_page.dart';
import 'package:crud/pages/Students/view_grades.dart';
import 'package:crud/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class SubjectsPage extends StatefulWidget {
  SubjectsPage(
      {Key key, this.model, this.isEditMode = true, SubjectModel subjectModel})
      : super(key: key);
  StudentModel model;
  SubjectModel subjectModel;
  bool isEditMode;

  @override
  _SubjectsPageState createState() => _SubjectsPageState();
}

class _SubjectsPageState extends State<SubjectsPage> {
  StudentModel model;
  SubjectModel subjectModel;

  DBService dbService;
  bool science = false;
  bool physics = false;
  bool genCal = false;
  bool java = false;
  bool appDev = false;
  // bool value = false;
  List data = [
    'Science',
    'Physics',
    'Calculus',
    'Computer Programming',
    'Applications Development'
  ];
  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new StudentModel();
    subjectModel = new SubjectModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  // Widget buildCheckBox() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.science = !science;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: science,
  //         onChanged: (value) {
  //           setState(() {
  //             this.science = value;
  //           });
  //         },
  //       ),
  //       title: Text('Science'),
  //     );

  // Widget buildCheckBox2() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.physics = !physics;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: physics,
  //         onChanged: (value) {
  //           setState(() {
  //             this.physics = value;
  //           });
  //         },
  //       ),
  //       title: Text('Physics'),
  //     );

  // Widget buildCheckBox3() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.genCal = !genCal;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: genCal,
  //         onChanged: (value) {
  //           setState(() {
  //             this.genCal = value;
  //           });
  //         },
  //       ),
  //       title: Text('General Calculus'),
  //     );
  // Widget buildCheckBox4() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.java = !java;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: java,
  //         onChanged: (value) {
  //           setState(() {
  //             this.java = value;
  //           });
  //         },
  //       ),
  //       title: Text('Java'),
  //     );

  // Widget buildCheckBox5() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.appDev = !appDev;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: appDev,
  //         onChanged: (value) {
  //           setState(() {
  //             this.appDev = value;
  //           });
  //         },
  //       ),
  //       title: Text('Applications Development'),
  //     );

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.studentName);
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 143, 236, 146),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.green,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.studentName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Student Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => StudentHome(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.fact_check,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Enrollment',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => EnrollmentPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
                color: Colors.white,
              ),
              tileColor: Colors.green,
              title: const Text(
                'Subjects',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SubjectsPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Container(
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 86, 138, 99),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Icon(
                  Icons.text_increase_outlined,
                  color: Color.fromARGB(255, 143, 236, 146),
                ),
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Grades',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ViewGradesPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 250,
            ),
            ListTile(
              leading: Icon(
                Icons.logout_sharp,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green,
        automaticallyImplyLeading: true,
        title: Text("Subjects"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    subjectModel.studentId = model.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.green,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/subjects.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<SubjectModel>>(
              future: dbService.getSubjects(model.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<SubjectModel>> subjects) {
                if (subjects.hasData) {
                  return _buildUI(subjects.data);
                }

                return Text('No Subjects Enrolled');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<SubjectModel> subjects) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(subjects)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(List<SubjectModel> subjectModel) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Subject\nName",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Subject\nCode",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Room\nNumber",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Professor",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Schedule",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: subjectModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.subjectName}',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.subjectCode}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.roomNum}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.professor}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.schedule}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  // DataCell(
                  //   Center(
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //       children: <Widget>[
                  //         new IconButton(
                  //           padding: EdgeInsets.all(0),
                  //           icon: Icon(Icons.delete),
                  //           color: Colors.red,
                  //           onPressed: () {
                  //             FormHelper.showMessage(
                  //               context,
                  //               "SQFLITE CRUD",
                  //               "Do you want to delete this record?",
                  //               "Yes",
                  //               () {
                  //                 dbService.deleteSubject(data).then((value) {
                  //                   setState(() {
                  //                     Navigator.of(context).pop();
                  //                   });
                  //                 });
                  //               },
                  //               buttonText2: "No",
                  //               isConfirmationDialog: true,
                  //               onPressed2: () {
                  //                 Navigator.of(context).pop();
                  //               },
                  //             );
                  //           },
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  // Widget buildeCheckBox2() => ListTile(
  //   onTap: () {
  //     setState(() {
  //       this.science = !science;
  //     });
  //   },
  //   trailing: Checkbox(
  //     value: science,
  //     onChanged: (value) {
  //       setState(() {
  //         this.science = value;
  //       });
  //     },
  //   ),
  //   title: Text('Physics'),
  // );

  // Widget _formUI() {
  //   return SingleChildScrollView(
  //     child: Padding(
  //       padding: EdgeInsets.all(10),
  //       child: Container(
  //         child: Align(
  //           alignment: Alignment.topLeft,
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             children: [
  //               // buildCheckBox(),
  //               // buildCheckBox2(),
  //               // buildCheckBox3(),
  //               // buildCheckBox4(),
  //               // buildCheckBox5(),

  //               // Checkbox(
  //               //   value: value,
  //               //   onChanged: (bool newValue) {
  //               //     setState(() {
  //               //       value = newValue;
  //               //     });
  //               //   },
  //               // ),

  //               // CheckboxListTile(
  //               //   title: Text('Science'),
  //               //   value: value,
  //               // onChanged: (bool newValue) {
  //               //   setState(() {
  //               //     value = newValue;
  //               //   });
  //               // },

  //               // ),
  //               Column(
  //                   children: data.map((e) {
  //                 return CheckboxListTile(
  //                     title: Text(e),
  //                     value: selectedData.indexOf(e) < 0 ? false : true,
  //                     onChanged: (bool newValue) {
  //                       if (selectedData.indexOf(e) < 0) {
  //                         setState(() {
  //                           selectedData.add(e);
  //                         });
  //                       } else {
  //                         setState(() {
  //                           selectedData.removeWhere((element) => element == e);
  //                         });
  //                       }
  //                       print(selectedData);
  //                     });
  //               }).toList()),

  //               SizedBox(
  //                 height: 30,
  //               ),

  //               btnSubmit(),
  //             ],
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // Widget _sectionCategory() {
  //   return FutureBuilder<List<CategoryModel>>(
  //     future: dbService.getCategories(),
  //     builder: (BuildContext context,
  //         AsyncSnapshot<List<CategoryModel>> categories) {
  //       if (categories.hasData) {
  //         return FormHelper.selectDropdown(
  //           context,
  //           model.sectionId,
  //           categories.data,
  //           (value) => {this.model.sectionId = int.parse(value)},
  //           onValidate: (value) {
  //             if (value == null) {
  //               // return 'Please enter student section';

  //             }
  //             return null;
  //           },
  //         );
  //       }

  //       return CircularProgressIndicator();
  //     },
  //   );
  // }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.green,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          // for (int x = 0; x < selectedData.length; x++) {
          // subjectModel.subjectName = selectedData[x];
          // subjectModel.studentId = model.id;
          subjectModel.studentId = model.id;
          subjectModel.subjectNames = selectedData;
          dbService.studentSubjects(subjectModel);
          // }
          // print(science);
          // if (science == true) {
          //   subjectModel1.subjectName = 'Science';
          //   subjectModel1.studentId = model.id;

          //   dbService.studentSubjects(subjectModel1);
          // }

          // if (physics == true) {
          //   subjectModel2.subjectName = 'Physics';
          //   subjectModel2.studentId = model.id;

          //   dbService.studentSubjects(subjectModel2);
          // }

          // if (genCal == true) {
          //   subjectModel.subjectName = 'General Calculus';
          //   subjectModel.studentId = model.id;

          //   dbService.studentSubjects(subjectModel);
          // }

          FormHelper.showMessage(
            context,
            "Add Subject/s",
            "${selectedData.length} subject/s successfully enrolled.",
            "Ok",
            () {
              Navigator.pop(context, true);
            },
          );
        },

        // {
        //   if (validateAndSave()) {
        //     print(model.toMap());

        //     dbService.updateStudent(model).then((value) {
        //       FormHelper.showMessage(
        //         context,
        //         "Update Student Record",
        //         "Student record updated successfully",
        //         "Ok",
        //         () {
        //           Navigator.pop(context, true);
        //         },
        //       );
        //     });
        //   }
        // },
        child: Text('Add Subject'),
      ),
    );
  }
}
