// @dart=2.9
import 'dart:io';

import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/Students/enrollment_page.dart';
import 'package:crud/pages/Students/subject_page.dart';
import 'package:crud/pages/Students/view_grades.dart';
import 'package:crud/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class StudentHome extends StatefulWidget {
  StudentHome({Key key, this.model, this.isEditMode = true}) : super(key: key);
  StudentModel model;
  bool isEditMode;

  @override
  _StudentHomeState createState() => _StudentHomeState();
}

class _StudentHomeState extends State<StudentHome> {
  StudentModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new StudentModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.studentName);
    // ignore: unnecessary_new
    // String x = "Stephen Ricard";
    // String[] nameparts = x.split(" ");
    // String initials = nameparts[0].charAt(0).toUpperCase()+nameparts[1].charAt(0).toUpperCase();
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 143, 236, 146),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.green,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.studentName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.white,
              ),
              tileColor: Colors.green,
              title: const Text(
                'Student Profile',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => StudentHome(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.fact_check,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Enrollment',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => EnrollmentPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Subjects',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SubjectsPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Container(
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 86, 138, 99),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Icon(
                  Icons.text_increase_outlined,
                  color: Color.fromARGB(255, 143, 236, 146),
                ),
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Grades',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ViewGradesPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 250,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green,
        automaticallyImplyLeading: true,
        title: Text("Student Profile"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormHelper.picPicker(
                  model.studentPic,
                  (file) => {
                    setState(
                      () {
                        model.studentPic = file.path;
                      },
                    )
                  },
                ),
                SizedBox(
                  height: 20,
                ),

                FormHelper.textInputEmail(
                  context,
                  model.email,
                  (value) => {
                    this.model.email = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter Student Name.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Email should be in this format email@gmail.com';
                    }
                    return null;
                  },
                ),

                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputPassword(
                  context,
                  model.password,
                  (value) => {
                    this.model.password = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter password.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Password should have uppercase, lowercase, numeric and special character.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputName(
                  context,
                  model.studentName,
                  (value) => {
                    this.model.studentName = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Student Name.';
                    } else if (value.toString().length < 8) {
                      return 'Name should be at least 8 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputAddress(
                  context,
                  model.address,
                  (value) => {
                    this.model.address = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Address.';
                    } else if (value.toString().length < 10) {
                      return 'Address should be at least 10 characters.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                // FormHelper.fieldLabel("Address"),
                // FormHelper.textInput(
                //     context,
                //     model.address,
                //     (value) => {
                //           this.model.address = value,
                //         },
                //     isTextArea: true, onValidate: (value) {
                //   return null;
                // }),

                FormHelper.textInputBday(
                  context,
                  model.bDay,
                  (value) => {
                    this.model.bDay = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$)';

                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter birthday.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Birthday should be in this format MM/DD/YYYY.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputContact(
                  context,
                  model.contact,
                  (value) => {
                    this.model.contact = value,
                  },
                  isNumberInput: true,
                  onValidate: (value) {
                    String pattern = r'(^(09|\+639)\d{9}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter contact number.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Contact number should be in this format 09#########.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputGender(
                  context,
                  model.gender,
                  (value) => {
                    this.model.gender = value,
                  },
                  onValidate: (value) {
                    String pattern = r'(^(?:m|M|male|Male|f|F|female|Female)$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter gender.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Gender should be Male or Female.';
                    }

                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                _sectionCategory(),
                SizedBox(
                  height: 30,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _sectionCategory() {
    return FutureBuilder<List<CategoryModel>>(
      future: dbService.getCategories(),
      builder: (BuildContext context,
          AsyncSnapshot<List<CategoryModel>> categories) {
        if (categories.hasData) {
          return FormHelper.selectDropdown(
            context,
            model.sectionId,
            categories.data,
            (value) => {this.model.sectionId = int.parse(value)},
            onValidate: (value) {
              if (value == null) {
                return 'Please enter student section';
              }
              return null;
            },
          );
        }

        return CircularProgressIndicator();
      },
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.green,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());

            dbService.updateStudent(model).then((value) {
              FormHelper.showMessage(
                context,
                "Update Student Record",
                "Student record updated successfully",
                "Ok",
                () {
                  Navigator.pop(context, true);
                },
              );
            });
          }
        },
        child: Text('Change Profile Information'),
      ),
    );
  }
}
