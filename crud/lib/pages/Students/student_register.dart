// @dart=2.9
import 'package:crud/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class RegisterStudent extends StatefulWidget {
  RegisterStudent({Key key, this.model, this.isEditMode = false})
      : super(key: key);
  StudentModel model;
  bool isEditMode;

  @override
  _RegisterStudentState createState() => _RegisterStudentState();
}

class _RegisterStudentState extends State<RegisterStudent> {
  StudentModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new StudentModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Registration'),
        backgroundColor: Colors.green,
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    TextEditingController confrimPassword = TextEditingController();
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Align(
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    child: SingleChildScrollView(
                      child: Container(
                        width: 450,
                        height: 300,
                        padding: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.green,
                              Colors.white,
                            ],
                          ),
                        ),
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: Image.asset(
                            'assets/images/register.png',
                          ),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    'Register',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ],
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      FormHelper.fieldLabel(
                        "Select Student Photo",
                      ),
                      FormHelper.picPicker(
                        model.studentPic,
                        (file) => {
                          setState(
                            () {
                              model.studentPic = file.path;
                            },
                          )
                        },
                      ),

                      SizedBox(
                        height: 20,
                      ),

                      FormHelper.textInputEmail(
                        context,
                        model.email,
                        (value) => {
                          this.model.email = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter Student Name.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Email should be in this format email@gmail.com';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputPassword(
                        context,
                        model.password,
                        (value) => {
                          this.model.password = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter password.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Password should have uppercase, lowercase, numeric and special character.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      TextFormField(
                        obscureText: true,
                        controller: confrimPassword,
                        decoration: InputDecoration(
                          hintText: 'Confirm Password',
                          labelText: 'Confirm Password',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please confirm your password.';
                          } else if (confrimPassword.text != model.password) {
                            return 'Password mismatch.';
                          } else {
                            return null;
                          }
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputName(
                        context,
                        model.studentName,
                        (value) => {
                          this.model.studentName = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Student Name.';
                          } else if (value.toString().length < 8) {
                            return 'Name should be at least 8 characters';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputAddress(
                        context,
                        model.address,
                        (value) => {
                          this.model.address = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Address.';
                          } else if (value.toString().length < 10) {
                            return 'Address should be at least 10 characters.';
                          }
                          return null;
                        },
                      ),

                      // FormHelper.fieldLabel("Address"),
                      // FormHelper.textInput(
                      //     context,
                      //     model.address,
                      //     (value) => {
                      //           this.model.address = value,
                      //         },
                      //     isTextArea: true, onValidate: (value) {
                      //   return null;
                      // }),

                      SizedBox(
                        height: 10,
                      ),

                      FormHelper.textInputBday(
                        context,
                        model.bDay,
                        (value) => {
                          this.model.bDay = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$)';

                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter birthday.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Birthday should be in this format MM/DD/YYYY.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      FormHelper.textInputContact(
                        context,
                        model.contact,
                        (value) => {
                          this.model.contact = value,
                        },
                        isNumberInput: true,
                        onValidate: (value) {
                          String pattern = r'(^(09|\+639)\d{9}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter contact number.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Contact number should be in this format 09#########.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      FormHelper.textInputGender(
                        context,
                        model.gender,
                        (value) => {
                          this.model.gender = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(?:m|M|male|Male|f|F|female|Female)$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter gender.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Gender should be Male or Female.';
                          }

                          return null;
                        },
                      ),

                      btnSubmit(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _sectionCategory() {
    return FutureBuilder<List<CategoryModel>>(
      future: dbService.getCategories(),
      builder: (BuildContext context,
          AsyncSnapshot<List<CategoryModel>> categories) {
        if (categories.hasData) {
          return FormHelper.selectDropdown(
            context,
            model.sectionId,
            categories.data,
            (value) => {this.model.sectionId = int.parse(value)},
            onValidate: (value) {
              if (value == null) {
                return 'Please enter student section';
              }
              return null;
            },
          );
        }

        return CircularProgressIndicator();
      },
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.green,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());
            if (widget.isEditMode) {
              dbService.updateStudent(model).then((value) {
                FormHelper.showMessage(
                  context,
                  "Update Record",
                  "Student record updated successfully",
                  "Ok",
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PendingRegistrationPage(),
                      ),
                    );
                  },
                );
              });
            } else {
              dbService.registerStudent(model).then((value) {
                FormHelper.showMessage(
                  context,
                  "Registration",
                  "Student registered successfully",
                  "Ok",
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ),
                    );
                  },
                );
              });
            }
          }
        },
        child: Text('Register'),
      ),
    );
  }
}
