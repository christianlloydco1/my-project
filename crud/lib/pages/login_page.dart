// @dart=2.9
import 'package:crud/Comm/comHelper.dart';
import 'package:crud/Comm/genTextFormField.dart';
import 'package:crud/pages/Admin/students_information.dart';
import 'package:crud/pages/Students/student_home.dart';
import 'package:crud/pages/Students/student_register.dart';
import 'package:crud/utils/db_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crud/models/data_model.dart';
import 'package:crud/pages/Admin/pending_registration.dart';
import 'package:crud/services/db_service.dart';
import 'package:crud/utils/form_helper.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.model, this.isEditMode = false}) : super(key: key);
  StudentModel model;
  bool isEditMode;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  StudentModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  final _conEmail = TextEditingController();
  final _conPassword = TextEditingController();
  DB dbHelper;

  @override
  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new StudentModel();
    dbHelper = DB();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  login() async {
    String uid = _conEmail.text;
    String passwd = _conPassword.text;

    if (uid.isEmpty) {
      alertDialog(context, "Please Enter email");
    } else if (passwd.isEmpty) {
      alertDialog(context, "Please Enter Password");
    } else {
      await dbHelper.getLoginUser(uid, passwd).then((userData) {
        if (userData != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => StudentHome(
                isEditMode: true,
                model: userData,
              ),
            ),
          );
        } else {
          alertDialog(context, "Error: User Not Found");
        }
      }).catchError((error) {
        print(error);
        alertDialog(context, "Error: Login Fail");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //   centerTitle: true,
      //   backgroundColor: Colors.green,
      //   automaticallyImplyLeading: true,
      //   title: Text(widget.isEditMode ? "Edit Student" : "Add Student"),
      // ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Container(
        child: Align(
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    child: SingleChildScrollView(
                      child: Container(
                        width: 430,
                        height: 300,
                        padding: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.green,
                              Colors.white,
                            ],
                          ),
                        ),
                        child: Image.asset(
                          'assets/images/login.png',
                        ),
                      ),
                    ),
                  ),
                  Text(
                    'Login',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  )
                ],
              ),

              SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  child: Column(
                    children: [
                      _fetchData(),
                      SizedBox(
                        height: 20,
                      ),
                      registerBtnSubmit(),
                      SizedBox(
                        height: 20,
                      ),
                      studentBtnSubmitAdmin(),
                    ],
                  ),
                ),
              ))

              // FormHelper.fieldLabel("Address"),
              // FormHelper.textInput(
              //     context,
              //     model.address,
              //     (value) => {
              //           this.model.address = value,
              //         },
              //     isTextArea: true, onValidate: (value) {
              //   return null;
              // }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _sectionCategory() {
    return FutureBuilder<List<CategoryModel>>(
      future: dbService.getCategories(),
      builder: (BuildContext context,
          AsyncSnapshot<List<CategoryModel>> categories) {
        if (categories.hasData) {
          return FormHelper.selectDropdown(
            context,
            model.sectionId,
            categories.data,
            (value) => {this.model.sectionId = int.parse(value)},
            onValidate: (value) {
              if (value == null) {
                return 'Please enter student section';
              }
              return null;
            },
          );
        }

        return CircularProgressIndicator();
      },
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // genLoginSignupHeader('Login'),
                      getTextFormField(
                          controller: _conEmail,
                          icon: Icons.person,
                          hintName: 'Email'),
                      SizedBox(height: 10.0),
                      getTextFormField(
                        controller: _conPassword,
                        icon: Icons.lock,
                        hintName: 'Password',
                        isObscureText: true,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('Does not have account? '),
                            FlatButton(
                              textColor: Colors.blue,
                              child: Text('Register'),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => RegisterStudent()));
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.green,
                    Colors.white,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget registerBtnSubmit() {
    return new Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: 380,
          height: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: login,
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Colors.green,
                      Color.fromARGB(255, 139, 241, 92)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  width: 400,
                  height: 100,
                  alignment: Alignment.center,
                  child: const Text(
                    'Login as student',
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
        // child: ElevatedButton(
        //   style: ElevatedButton.styleFrom(
        //       primary: ,

        //       padding: EdgeInsets.zero,
        //       shape: RoundedRectangleBorder(
        //           borderRadius: BorderRadius.circular(20))),
        //   onPressed: () {
        //     if (validateAndSave()) {
        //       print(model.toMap());
        //       if (widget.isEditMode) {
        //         dbService.updateStudent(model).then((value) {
        //           FormHelper.showMessage(
        //             context,
        //             "Update Record",
        //             "Student record updated successfully",
        //             "Ok",
        //             () {
        //               Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                   builder: (context) => HomePage(),
        //                 ),
        //               );
        //             },
        //           );
        //         });
        //       } else {
        //         dbService.addStudent(model).then((value) {
        //           FormHelper.showMessage(
        //             context,
        //             "Add Student",
        //             "Student record added successfully",
        //             "Ok",
        //             () {
        //               Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                   builder: (context) => HomePage(),
        //                 ),
        //               );
        //             },
        //           );
        //         });
        //       }
        //     }
        //   },
        //   child: Text(
        //     'Save Student',
        //     style: TextStyle(
        //       fontSize: 20,
        //       fontFamily: 'Poppins',
        //       fontWeight: FontWeight.bold,
        //     ),
        //   ),
        // ),
        );
  }

  Widget studentBtnSubmitAdmin() {
    return new Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: 380,
          height: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                print(model.toMap());

                if (_conEmail.text.isEmpty) {
                  alertDialog(context, "Please Enter email");
                } else if (_conEmail.text.isEmpty) {
                  alertDialog(context, "Please Enter email");
                } else if (_conPassword.text.isEmpty) {
                  alertDialog(context, "Please Enter Password");
                } else if (_conEmail.value.text == 'admin@gmail.com' &&
                    _conPassword.value.text == '123') {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StudentsInformationPage(),
                    ),
                  );
                } else {
                  alertDialog(context, "Error: User Not Found");
                }

                // else {
                //   dbService.addStudent(model).then((value) {
                //     FormHelper.showMessage(
                //       context,
                //       "Add Student",
                //       "Student record added successfully",
                //       "Ok",
                //       () {
                //         Navigator.push(
                //           context,
                //           MaterialPageRoute(
                //             builder: (context) => PendingRegistrationPage(),
                //           ),
                //         );
                //       },
                //     );
                //   });
                // }
              },
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Color.fromARGB(255, 0, 81, 255),
                      Color.fromARGB(255, 64, 179, 247)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  width: 400,
                  height: 100,
                  alignment: Alignment.center,
                  child: const Text(
                    'Login as Admin',
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
        // child: ElevatedButton(
        //   style: ElevatedButton.styleFrom(
        //       primary: ,

        //       padding: EdgeInsets.zero,
        //       shape: RoundedRectangleBorder(
        //           borderRadius: BorderRadius.circular(20))),
        //   onPressed: () {
        //     if (validateAndSave()) {
        //       print(model.toMap());
        //       if (widget.isEditMode) {
        //         dbService.updateStudent(model).then((value) {
        //           FormHelper.showMessage(
        //             context,
        //             "Update Record",
        //             "Student record updated successfully",
        //             "Ok",
        //             () {
        //               Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                   builder: (context) => HomePage(),
        //                 ),
        //               );
        //             },
        //           );
        //         });
        //       } else {
        //         dbService.addStudent(model).then((value) {
        //           FormHelper.showMessage(
        //             context,
        //             "Add Student",
        //             "Student record added successfully",
        //             "Ok",
        //             () {
        //               Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                   builder: (context) => HomePage(),
        //                 ),
        //               );
        //             },
        //           );
        //         });
        //       }
        //     }
        //   },
        //   child: Text(
        //     'Save Student',
        //     style: TextStyle(
        //       fontSize: 20,
        //       fontFamily: 'Poppins',
        //       fontWeight: FontWeight.bold,
        //     ),
        //   ),
        // ),
        );
  }

  // Widget btnRegister() {
  //   return new Align(
  //     alignment: Alignment.center,
  //     child: SizedBox(
  //       width: 380,
  //       height: 50,
  //       child: Center(
  //         child: TextButton(
  //             onPressed: () async {
  //               Navigator.of(context).pushReplacement(
  //                 MaterialPageRoute(builder: (context) => RegisterStudent()),
  //               );
  //             },
  //             child:
  //                 Text('Dont have Account yet? Register Student Account here')),
  //       ),
  //     ),

  //     // child: ElevatedButton(
  //     //   style: ElevatedButton.styleFrom(
  //     //       primary: ,

  //     //       padding: EdgeInsets.zero,
  //     //       shape: RoundedRectangleBorder(
  //     //           borderRadius: BorderRadius.circular(20))),
  //     //   onPressed: () {
  //     //     if (validateAndSave()) {
  //     //       print(model.toMap());
  //     //       if (widget.isEditMode) {
  //     //         dbService.updateStudent(model).then((value) {
  //     //           FormHelper.showMessage(
  //     //             context,
  //     //             "Update Record",
  //     //             "Student record updated successfully",
  //     //             "Ok",
  //     //             () {
  //     //               Navigator.push(
  //     //                 context,
  //     //                 MaterialPageRoute(
  //     //                   builder: (context) => HomePage(),
  //     //                 ),
  //     //               );
  //     //             },
  //     //           );
  //     //         });
  //     //       } else {
  //     //         dbService.addStudent(model).then((value) {
  //     //           FormHelper.showMessage(
  //     //             context,
  //     //             "Add Student",
  //     //             "Student record added successfully",
  //     //             "Ok",
  //     //             () {
  //     //               Navigator.push(
  //     //                 context,
  //     //                 MaterialPageRoute(
  //     //                   builder: (context) => HomePage(),
  //     //                 ),
  //     //               );
  //     //             },
  //     //           );
  //     //         });
  //     //       }
  //     //     }
  //     //   },
  //     //   child: Text(
  //     //     'Save Student',
  //     //     style: TextStyle(
  //     //       fontSize: 20,
  //     //       fontFamily: 'Poppins',
  //     //       fontWeight: FontWeight.bold,
  //     //     ),
  //     //   ),
  //     // ),
  //   );
  // }
}
