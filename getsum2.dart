import 'dart:io';

void main() {
  inputValues();
}

int inputValues() {
  print('Input first value:');
  int? a = int.parse(stdin.readLineSync()!);
  print('Input second value:');
  int? b = int.parse(stdin.readLineSync()!);
  outputValues(a, b);
  return (0);
}

int outputValues(int a, int b) {
  var c = getSum(a, b);
  print('The sum of $a and $b is $c');
  return (0);
}

int getSum(int a, int b) {
  return (a + b);
}
