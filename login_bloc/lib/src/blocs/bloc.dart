import 'dart:async';
import 'validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc extends Object with Validators {
  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();
  final _name = BehaviorSubject<String>();

  //Retrieve data
  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<String> get password => _password.stream.transform(validatePassword);
  Stream<String> get name => _name.stream.transform(validateName);
  Stream<bool> get submitValid =>
      Rx.combineLatest3(email, password, name, (e, p, n) => true);
  //Add data to stream
  Function(String) get changeEmail => _email.sink.add;
  Function(String) get changePassword => _password.sink.add;
  Function(String) get changeName => _name.sink.add;

  submit() {
    final validEmail = _email.value;
    final validPassword = _password.value;
    final validName = _name.value;

    print('email is $validEmail');
    print('password is $validPassword');
    print('Name is $validName');
  }

  dispose() {
    _email.close();
    _password.close();
  }
}
