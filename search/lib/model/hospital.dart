class Hospital {
  final String hospitalName;
  final String address;
  final String image;
  final String specialization;
  final String price;

  const Hospital(
      {required this.hospitalName,
      required this.address,
      required this.image,
      required this.specialization,
      required this.price});

  factory Hospital.fromJson(Map<String, dynamic> json) => Hospital(
        hospitalName: json['hospitalName'],
        address: json['address'],
        image: json['image'],
        specialization: json['specialization'],
        price: json['price'].toString(),
      );

  Map<String, dynamic> toJson() => {
        'hospitalName': hospitalName,
        'address': address,
        'image': image,
        'specialization': specialization,
        'price': price,
      };
}
