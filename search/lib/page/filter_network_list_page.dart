import 'dart:async';

import 'package:search/api/hospitals_api.dart';
import 'package:search/main.dart';
import 'package:search/model/hospital.dart';
import 'package:search/page/hospital_page.dart';
import 'package:search/widget/search_price.dart';
import 'package:search/widget/search_widget.dart';
import 'package:flutter/material.dart';

class FilterNetworkListPage extends StatefulWidget {
  @override
  FilterNetworkListPageState createState() => FilterNetworkListPageState();
}

class FilterNetworkListPageState extends State<FilterNetworkListPage> {
  List<Hospital> hospitals = [];
  String query = '';
  String minPrice = '';
  String maxPrice = '';
  Timer? debouncer;

  @override
  void initState() {
    super.initState();

    init();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future init() async {
    final hospitals =
        await HospitalsApi.getHospitals(query, minPrice, maxPrice);

    setState(() => this.hospitals = hospitals);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(MyApp.title),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            buildSearch(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                buildSearchMinPrice(),
                buildSearchMaxPrice(),
              ],
            ),
            Text(
              'Number of Results ${hospitals.length}',
              style: TextStyle(fontSize: 15),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: hospitals.length,
                itemBuilder: (context, index) {
                  final hospital = hospitals[index];

                  return buildHospital(hospital);
                },
              ),
            ),
          ],
        ),
      );

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Name/Location/Specialization',
        onChanged: searchhospital,
      );
  Widget buildSearchMinPrice() => SearchPriceWidget(
        text: minPrice,
        hintText: 'Min Price',
        onChanged: searchhospitalMinPrice,
      );

  Widget buildSearchMaxPrice() => SearchPriceWidget(
        text: maxPrice,
        hintText: 'Max Price',
        onChanged: searchhospitalMaxPrice,
      );

  Future searchhospital(String query) async => debounce(() async {
        final hospitals =
            await HospitalsApi.getHospitals(query, minPrice, maxPrice);

        if (!mounted) return;

        setState(() {
          this.query = query;

          this.hospitals = hospitals;
        });
      });

  Future searchhospitalMinPrice(String minPrice) async => debounce(() async {
        final hospitals =
            await HospitalsApi.getHospitals(query, minPrice, maxPrice);

        if (!mounted) return;

        setState(() {
          this.minPrice = minPrice;

          this.hospitals = hospitals;
        });
      });

  Future searchhospitalMaxPrice(String maxPrice) async => debounce(() async {
        final hospitals =
            await HospitalsApi.getHospitals(query, minPrice, maxPrice);

        if (!mounted) return;

        setState(() {
          this.maxPrice = maxPrice;
          this.hospitals = hospitals;
        });
      });

  Widget buildHospital(Hospital hospital) => ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HospitalPage(
                model: hospital,
              ),
            ),
          );
        },
        leading: Image.network(
          hospital.image,
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        ),
        title: Text(hospital.hospitalName),
        subtitle: Text(hospital.address),
      );
}
