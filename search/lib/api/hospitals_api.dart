import 'dart:convert';

import 'package:search/model/hospital.dart';
import 'package:http/http.dart' as http;

class HospitalsApi {
  static Future<List<Hospital>> getHospitals(
      String query, String minPrice, String maxPrice) async {
    final url = Uri.parse('http://192.168.100.128:5500/lib/api/hospitals.json');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final List hospitals = json.decode(response.body);

      if (minPrice == '') {
        minPrice = '0';
      }

      if (maxPrice == '') {
        maxPrice = '0';
      }

      if (minPrice != '0' && maxPrice == '0') {
        print(minPrice);

        return hospitals
            .map((json) => Hospital.fromJson(json))
            .where((hospital) {
          final hospitalNameLower = hospital.hospitalName.toLowerCase();
          final addressLower = hospital.address.toLowerCase();
          final specializationLower = hospital.specialization.toLowerCase();
          final minPriceLower = hospital.price;

          final searchLower = query.toLowerCase();
          final double searchminPriceLower = double.parse(minPrice);

          return ((double.parse(minPriceLower) >= searchminPriceLower
                  // && double.parse(minPriceLower) <= 5000
                  ) &&
                  (hospitalNameLower.contains(searchLower) ||
                      addressLower.contains(searchLower) ||
                      specializationLower.contains(searchLower))

              // &&priceLower.contains(searchPriceLower)

              );
        }).toList();
      } else if (maxPrice != '0') {
        print(minPrice);
        print(maxPrice);
        return hospitals
            .map((json) => Hospital.fromJson(json))
            .where((hospital) {
          final hospitalNameLower = hospital.hospitalName.toLowerCase();
          final addressLower = hospital.address.toLowerCase();
          final specializationLower = hospital.specialization.toLowerCase();
          final minPriceLower = hospital.price;

          final searchLower = query.toLowerCase();
          final double searchminPriceLower = double.parse(minPrice);
          final double searchmaxPriceLower = double.parse(maxPrice);

          return ((double.parse(minPriceLower) >= searchminPriceLower &&
                      double.parse(minPriceLower) <= searchmaxPriceLower) &&
                  (hospitalNameLower.contains(searchLower) ||
                      addressLower.contains(searchLower) ||
                      specializationLower.contains(searchLower))

              // &&priceLower.contains(searchPriceLower)

              );
        }).toList();
      }

      return hospitals.map((json) => Hospital.fromJson(json)).where((hospital) {
        final hospitalNameLower = hospital.hospitalName.toLowerCase();
        final addressLower = hospital.address.toLowerCase();
        final specializationLower = hospital.specialization.toLowerCase();
        final minPriceLower = hospital.price;

        final searchLower = query.toLowerCase();
        final searchminPriceLower = minPrice.toLowerCase();

        return ((hospitalNameLower.contains(searchLower) ||
                addressLower.contains(searchLower) ||
                specializationLower.contains(searchLower))

            // &&priceLower.contains(searchPriceLower)

            );
      }).toList();
    } else {
      throw Exception();
    }
  }
}
