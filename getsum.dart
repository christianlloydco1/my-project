import 'dart:io';

void main() {
  print('Input first value:');
  int? a = int.parse(stdin.readLineSync()!);
  print('Input second value:');
  int? b = int.parse(stdin.readLineSync()!);
  var c = getSum(a, b);
  print('The sum of $a and $b is $c');
}

int getSum(int a, int b) {
  return (a + b);
}
