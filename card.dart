void main() {
  var deck = new Deck();
  print(deck);
  print(deck.deal(5));
  print(deck);
  print(deck.cardsWithSuit('Diamonds'));

  deck.removeCard('Ace', 'Diamonds');
}

class Deck {
  List<Card> cards = [];
  Deck() {
    var ranks = [
      'Ace',
      'Two',
      'Three',
      'Four',
      'Five',
      'Six',
      'Seven',
      'Eight',
      'Nine',
      'Ten',
      'Jack',
      'Queen',
      'King'
    ];
    var suits = ['Diamonds', 'Hearts', 'Clubs', 'Spades'];

    for (var rank in ranks) {
      for (var suit in suits) {
        var card = new Card(suit: suit, rank: rank);
        cards.add(card);
      }
    }
  }

  toString() {
    return cards.toString();
  }

  suffle() {
    cards.shuffle();
  }

  cardsWithSuit(String suit) {
    return cards.where((card) => card.suit == suit);
  }

  deal(int handSize) {
    var hand = cards.sublist(0, handSize);
    cards = cards.sublist(handSize);

    return hand;
  }

  removeCard(String rank, String suit) {
    return cards.removeWhere((card) => card.rank == rank && card.suit == suit);
  }
}

class Card {
  String? suit = '';
  String? rank = '';
  Card({this.rank, this.suit});

  toString() {
    return '$rank of $suit';
  }
}
