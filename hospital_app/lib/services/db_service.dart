// @dart=2.9
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/utils/db_helper.dart';

class DBService {
  Future<bool> addHospital(HospitalModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(HospitalModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> addServices(ServiceModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(ServiceModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> addAppointment(AppointmentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(AppointmentModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> addGeneralAppointment(GeneralAppointmentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted =
          await DB.insertAppointment(GeneralAppointmentModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> addRescheduleAppointment(GeneralAppointmentModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insertReschedule('reschedule', model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> addPatient(PatientModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(PatientModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> registerStudent(HospitalModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.insert(HospitalModel.registrationTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateHospital(HospitalModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.update(HospitalModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateAppointment(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();

    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateAppointment(
          GeneralAppointmentModel.table, model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateAppointmentOld(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();
    model.scheduleId = model.scheduleIdNew;
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateAppointmentOld(
          GeneralAppointmentModel.table, model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTime(
      GeneralAppointmentModel model, int origId) async {
    model.scheduleId = origId;
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateSchedule('schedules', model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTimeOld(GeneralAppointmentModel model) async {
    model.scheduleId = model.scheduleIdOld;
    print(model.scheduleId);
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateScheduleOld('schedules', model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTimeApproved(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();
    bool isSaved = false;
    model.timeRange = model.timeRangeOld;
    model.scheduleDate = model.scheduleDate;
    model.status = 'Open';
    if (model != null) {
      int inserted = await DB.updateSchedule('schedules', model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTimeApproved2(
      GeneralAppointmentModel model) async {
    await DB.init();
    bool isSaved = false;
    model.scheduleId = model.scheduleIdNew;
    model.timeRange = model.timeRangeNew;
    model.scheduleDate = model.scheduleDateNew;
    model.status = 'Scheduled';
    if (model != null) {
      int inserted = await DB.updateScheduleNew('schedules', model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTimeDenied(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();
    bool isSaved = false;
    model.scheduleId = model.scheduleIdOld;
    model.timeRange = model.timeRangeOld;
    model.scheduleDate = model.scheduleDateOld;
    model.status = 'Denied';
    if (model != null) {
      int inserted = await DB.updateScheduleDenied('schedules', model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateScheduleTimeDenied2(
      GeneralAppointmentModel model, int newId) async {
    await DB.init();
    bool isSaved = false;
    model.scheduleId = model.scheduleIdNew;
    model.timeRange = model.timeRangeNew;
    model.scheduleDate = model.scheduleDateOld;
    model.status = 'Open';
    print("sched id: ${model.scheduleIdNew}");
    if (model != null) {
      int inserted = await DB.updateScheduleDenied('schedules', model, newId);

      model.scheduleId = model.scheduleIdOld;
      print("sched id: ${model.scheduleId}");

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateRescheduleTimeApproved(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();
    bool isSaved = false;
    model.rescheduleStatus = 'Approved';
    if (model != null) {
      int inserted = await DB.updateReschedule('reschedule', model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateRescheduleTimeDenied(
      GeneralAppointmentModel model, int origId) async {
    await DB.init();
    bool isSaved = false;

    model.rescheduleStatus = 'Denied';
    if (model != null) {
      int inserted = await DB.updateReschedule('reschedule', model, origId);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updateServices(ServiceModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updateSpecificService(ServiceModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> updatePatient(PatientModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.updatePatient(PatientModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<List<HospitalModel>> getHospitals() async {
    await DB.init();
    List<Map<String, dynamic>> students = await DB.query(HospitalModel.table);

    return students.map((item) => HospitalModel.fromMap(item)).toList();
  }

  Future<List<PatientModel>> getPatients() async {
    await DB.init();
    List<Map<String, dynamic>> students = await DB.query(PatientModel.table);

    return students.map((item) => PatientModel.fromMap(item)).toList();
  }

  Future<List<ServiceModel>> getServices(int id) async {
    await DB.init();
    List<Map<String, dynamic>> services =
        await DB.queryServices(ServiceModel.table, id);

    return services.map((item) => ServiceModel.fromMap(item)).toList();
  }

  Future<List<GeneralAppointmentModel>> getGeneralAppointment(
      int id, String date) async {
    await DB.init();
    List<Map<String, dynamic>> services = await DB.queryGeneralAppointment(
        GeneralAppointmentModel.table, id, date);

    return services
        .map((item) => GeneralAppointmentModel.fromMap(item))
        .toList();
  }

  Future<List<HospitalModel>> getPendingStudents() async {
    await DB.init();
    List<Map<String, dynamic>> students =
        await DB.query(HospitalModel.registrationTable);

    return students.map((item) => HospitalModel.fromMap(item)).toList();
  }

  Future<List<CategoryModel>> getCategories() async {
    await DB.init();
    List<Map<String, dynamic>> categories = await DB.query(CategoryModel.table);

    return categories.map((item) => CategoryModel.fromMap(item)).toList();
  }

  Future<List<ServiceModel>> getHospitalServices(int hospitalId) async {
    await DB.init();
    List<Map<String, dynamic>> categories =
        await DB.queryHospitalServices('services', hospitalId);

    return categories.map((item) => ServiceModel.fromMap(item)).toList();
  }

  Future<List<CategoryModel>> getServicePrice(int hospitalId) async {
    await DB.init();
    List<Map<String, dynamic>> categories =
        await DB.queryHospitalServices('services', hospitalId);

    return categories.map((item) => CategoryModel.fromMap(item)).toList();
  }

  Future<bool> deleteStudent(HospitalModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(HospitalModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> deleteService(ServiceModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.deleteSpecificService(ServiceModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> deletePatient(PatientModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(PatientModel.table, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }

  Future<bool> deleteStudentRegistration(HospitalModel model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(HospitalModel.registrationTable, model);

      isSaved = inserted == 1 ? true : false;
    }

    return isSaved;
  }
}
