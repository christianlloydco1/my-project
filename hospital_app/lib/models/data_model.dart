import 'package:hospital_app/models/model.dart';
import 'package:flutter/src/material/data_table.dart';

class PatientModel extends Model {
  static String registrationTable = 'registration';
  static String table = 'patients';

  int id;
  String email;
  String password;
  String patientName;
  String address;
  String bDay;
  String contact;
  String gender;
  String patientPic;

  PatientModel({
    required this.id,
    required this.email,
    required this.password,
    required this.patientName,
    required this.address,
    required this.bDay,
    required this.contact,
    required this.gender,
    required this.patientPic,
  });
  //fetching
  static PatientModel fromMap(Map<String, dynamic> map) {
    return PatientModel(
      email: map["email"],
      password: map["password"],
      id: map["patientId"],
      patientName: map['patientName'].toString(),
      address: map['address'],
      bDay: map['bDay'],
      contact: map['contact'].toString(),
      gender: map['gender'],
      patientPic: map['patientPic'],
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'patientId': id,
      'email': email,
      'password': password,
      'patientName': patientName,
      'address': address,
      'bDay': bDay,
      'contact': contact,
      'gender': gender,
      'patientPic': patientPic,
    };

    if (id != null) {
      map['patientId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class HospitalModel extends Model {
  static String registrationTable = 'registration';
  static String table = 'hospitals';

  int id;
  String email;
  String password;
  String hospitalName;
  String address;
  String contact;
  String hospitalPic;

  HospitalModel({
    required this.id,
    required this.email,
    required this.password,
    required this.hospitalName,
    required this.address,
    required this.contact,
    required this.hospitalPic,
  });
  //fetching
  static HospitalModel fromMap(Map<String, dynamic> map) {
    return HospitalModel(
      email: map["email"],
      password: map["password"],
      id: map["hospitalId"],
      hospitalName: map['hospitalName'].toString(),
      address: map['address'],
      contact: map['contact'].toString(),
      hospitalPic: map['hospitalPic'],
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'hospitalId': id,
      'email': email,
      'password': password,
      'hospitalName': hospitalName,
      'address': address,
      'contact': contact,
      'hospitalPic': hospitalPic,
    };

    if (id != null) {
      map['hospitalId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class ServiceModel extends Model {
  static String table = 'services';
  int serviceId;
  String serviceName;
  String price;
  int hospitalId;

  ServiceModel({
    required this.serviceId,
    required this.serviceName,
    required this.price,
    required this.hospitalId,
  });
  //fetching
  static ServiceModel fromMap(Map<String, dynamic> map) {
    return ServiceModel(
      serviceId: map["serviceId"],
      serviceName: map["serviceName"],
      price: map["price"].toString(),
      hospitalId: map['hospitalId'],
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'serviceId': serviceId,
      'serviceName': serviceName,
      'price': price,
      'hospitalId': hospitalId,
    };

    if (id != null) {
      map['serviceId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class AppointmentModel extends Model {
  static String table = 'appointments';
  int appointmentId;
  int patientId;
  int hospitalId;
  String serviceName;
  String price;

  AppointmentModel({
    required this.appointmentId,
    required this.patientId,
    required this.hospitalId,
    required this.serviceName,
    required this.price,
  });
  //fetching
  static AppointmentModel fromMap(Map<String, dynamic> map) {
    return AppointmentModel(
      appointmentId: map["appointmentId"],
      patientId: map['patientId'],
      hospitalId: map['hospitalId'],
      serviceName: map["serviceName"],
      price: map["price"].toString(),
    );
  }

  //inserting
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'appointmentId': appointmentId,
      'patientId': patientId,
      'hospitalId': hospitalId,
      'serviceName': serviceName,
      'price': price,
    };

    if (id != null) {
      map['appointmentId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class GeneralAppointmentModel extends Model {
  static String table = 'appointments';
  static String scheduleTable = 'schedules';

  int appointmentId;
  int patientId;
  int scheduleId;
  int hospitalId;
  int scheduleIdOld;
  int scheduleIdNew;
  int rescheduleId;
  String hospitalName;
  String patientName;
  String serviceName;
  String price;
  String scheduleDate;
  String scheduleDateNew;
  String scheduleDateOld;
  String timeRange;
  String timeRangeNew;
  String timeRangeOld;
  String status;
  String rescheduleStatus;

  GeneralAppointmentModel({
    required this.appointmentId,
    required this.patientId,
    required this.scheduleId,
    required this.hospitalId,
    required this.rescheduleId,
    required this.scheduleIdOld,
    required this.scheduleIdNew,
    required this.hospitalName,
    required this.patientName,
    required this.serviceName,
    required this.price,
    required this.scheduleDate,
    required this.scheduleDateNew,
    required this.scheduleDateOld,
    required this.timeRange,
    required this.timeRangeNew,
    required this.timeRangeOld,
    required this.status,
    required this.rescheduleStatus,
  });
  //fetching
  static GeneralAppointmentModel fromMap(Map<String, dynamic> map) {
    return GeneralAppointmentModel(
      appointmentId: map["appointmentId"],
      patientId: map['patientId'],
      scheduleId: map['scheduleId'],
      hospitalId: map['hospitalId'],
      scheduleIdOld: map['scheduleIdOld'],
      scheduleIdNew: map['scheduleIdNew'],
      rescheduleId: map['rescheduleId'],
      hospitalName: map['hospitalName'],
      patientName: map['patientName'],
      serviceName: map["serviceName"],
      price: map["price"].toString(),
      scheduleDate: map["scheduleDate"].toString(),
      scheduleDateNew: map["scheduleDateNew"].toString(),
      scheduleDateOld: map["scheduleDateOld"].toString(),
      timeRange: map["timeRange"].toString(),
      timeRangeOld: map["timeRangeOld"],
      timeRangeNew: map["timeRangeNew"].toString(),
      status: map["status"].toString(),
      rescheduleStatus: map["rescheduleStatus"].toString(),
    );
  }

  //inserting
  Map<String, dynamic> toMapAppointments() {
    Map<String, dynamic> map = {
      'appointmentId': appointmentId,
      'patientId': patientId,
      'hospitalId': hospitalId,
      'serviceName': serviceName,
      'price': price,
      'scheduleId': scheduleId,
    };

    if (id != null) {
      map['appointmentId'] = id;
    }
    return map;
  }

  Map<String, dynamic> toMapSchedules() {
    Map<String, dynamic> map = {
      'scheduleId': scheduleId,
      'timeRange': timeRange,
      'status': status,
      'scheduleDate': scheduleDate,
      'hospitalId': hospitalId,
    };

    if (id != null) {
      map['appointmentId'] = id;
    }
    return map;
  }

  Map<String, dynamic> toMapReschedule() {
    Map<String, dynamic> map = {
      'scheduleIdOld': scheduleIdOld,
      'rescheduleId': rescheduleId,
      'timeRangeOld': timeRangeOld,
      'timeRangeNew': timeRangeNew,
      'rescheduleStatus': rescheduleStatus,
      'scheduleDateOld': scheduleDateOld,
      'scheduleDateNew': scheduleDateNew,
      'hospitalId': hospitalId,
      'scheduleIdNew': scheduleIdNew,
    };

    if (id != null) {
      map['appointmentId'] = id;
    }
    return map;
  }

  map(DataRow Function(dynamic data) param0) {}
}

class CategoryModel extends Model {
  static String table = 'services';

  String serviceName;
  String price;

  int serviceId;

  CategoryModel({
    required this.serviceId,
    required this.serviceName,
    required this.price,
  });

  static CategoryModel fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      serviceId: map["serviceId"],
      serviceName: map['serviceName'],
      price: map['price'].toString(),
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'serviceName': serviceName,
      'price': price,
    };

    if (id != null) {
      map['serviceId'] = id;
    }
    return map;
  }
}

// class CountModel extends Model {
//   static String table = 'appointments';
//   int count;
//   int appointmentId;
//   int hospitalId;

//   String status;

//   CountModel({
//     required this.count,
//     required this.appointmentId,
//     required this.hospitalId,
//     required this.status,
//   });
//   //fetching
//   static CountModel fromMap(Map<String, dynamic> map) {
//     return CountModel(
//       count: map["COUNT (a.hospitalId)"],
//       appointmentId: map["appointmentId"],
//       hospitalId: map['hospitalId'],
//       status: map["status"].toString(),
//     );
//   }

//   //inserting
//   Map<String, dynamic> toMap() {
//     Map<String, dynamic> map = {
//       'appointmentId': appointmentId,
//       'hospitalId': hospitalId,
//       'status': status,
//     };

//     if (id != null) {
//       map['appointmentId'] = id;
//     }
//     return map;
//   }

//   map(DataRow Function(dynamic data) param0) {}
// }

class PriceModel extends Model {
  static String table = 'services';

  double price;
  int serviceId;

  PriceModel({
    required this.serviceId,
    required this.price,
  });

  static PriceModel fromMap(Map<String, dynamic> map) {
    return PriceModel(
      serviceId: map["serviceId"],
      price: map['price'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'price': price,
    };

    if (id != null) {
      map['serviceId'] = id;
    }
    return map;
  }
}
