// @dart=2.9
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/add_services.dart';
import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:hospital_app/pages/on_boarding.dart';
import 'package:hospital_app/pages/Hospitals/hospital_register.dart';
import 'package:flutter/material.dart';
import 'pages/Admin/patients_information.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQFLITE hospital_app',
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
