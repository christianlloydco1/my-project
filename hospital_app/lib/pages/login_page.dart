// @dart=2.9
import 'package:hospital_app/Comm/comHelper.dart';
import 'package:hospital_app/Comm/genTextFormField.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/Hospitals/hospital_register.dart';
import 'package:hospital_app/pages/Patients/patient_register.dart';
import 'package:hospital_app/pages/Patients/patients_home.dart';
import 'package:hospital_app/utils/db_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class LoginPage extends StatefulWidget {
  LoginPage(
      {Key key, this.hospitalModel, this.patientModel, this.isEditMode = false})
      : super(key: key);
  HospitalModel hospitalModel;
  PatientModel patientModel;
  bool isEditMode;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  HospitalModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  final _conEmail = TextEditingController();
  final _conPassword = TextEditingController();
  DB dbHelper;

  @override
  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new HospitalModel();
    dbHelper = DB();

    if (widget.isEditMode) {
      model = widget.hospitalModel;
    }
  }

  loginPatient() async {
    String uid = _conEmail.text;
    String passwd = _conPassword.text;

    if (uid.isEmpty) {
      alertDialog(context, "Please Enter email");
    } else if (passwd.isEmpty) {
      alertDialog(context, "Please Enter Password");
    } else {
      await dbHelper.getLoginPatient(uid, passwd).then((userData) {
        if (userData != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PatientHome(
                isEditMode: true,
                patientModel: userData,
              ),
            ),
          );
        } else {
          alertDialog(context, "Error: User Not Found");
        }
      }).catchError((error) {
        print(error);
        alertDialog(context, "Error: Login Fail");
      });
    }
  }

  loginHospital() async {
    String uid = _conEmail.text;
    String passwd = _conPassword.text;

    if (uid.isEmpty) {
      alertDialog(context, "Please Enter email");
    } else if (passwd.isEmpty) {
      alertDialog(context, "Please Enter Password");
    } else {
      await dbHelper.getLoginUser(uid, passwd).then((userData) {
        if (userData != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HospitalHome(
                isEditMode: true,
                model: userData,
              ),
            ),
          );
        } else {
          alertDialog(context, "Error: User Not Found");
        }
      }).catchError((error) {
        print(error);
        alertDialog(context, "Error: Login Fail");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //   centerTitle: true,
      //   backgroundColor: Colors.blue,
      //   automaticallyImplyLeading: true,
      //   title: Text(widget.isEditMode ? "Edit Student" : "Add Student"),
      // ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Container(
        child: Align(
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    child: SingleChildScrollView(
                      child: Container(
                        width: 430,
                        height: 300,
                        padding: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.blue,
                              Colors.white,
                            ],
                          ),
                        ),
                        child: Image.asset(
                          'assets/images/login.png',
                        ),
                      ),
                    ),
                  ),
                  Text(
                    'Login',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  )
                ],
              ),
              SingleChildScrollView(
                  child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  child: Column(
                    children: [
                      _fetchData(),
                      SizedBox(
                        height: 20,
                      ),
                      btnSubmitPatient(),
                      SizedBox(
                        height: 20,
                      ),
                      btnSubmitHospital(),
                      SizedBox(
                        height: 20,
                      ),
                      btnSubmitAdmin(),
                    ],
                  ),
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // genLoginSignupHeader('Login'),
                      getTextFormField(
                          controller: _conEmail,
                          icon: Icons.person,
                          hintName: 'Email'),
                      SizedBox(height: 10.0),
                      getTextFormField(
                        controller: _conPassword,
                        icon: Icons.lock,
                        hintName: 'Password',
                        isObscureText: false,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Text('Does not have account? '),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FlatButton(
                                  textColor: Colors.blue,
                                  child: Text('Register Patient'),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => RegisterPatient()));
                                  },
                                ),
                                FlatButton(
                                  textColor: Colors.blue,
                                  child: Text('Register Hospital'),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) =>
                                                RegisterHospital()));
                                  },
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmitPatient() {
    return new Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: 350,
          height: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: loginPatient,
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Colors.green,
                      Color.fromARGB(255, 139, 241, 92)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  width: 400,
                  height: 100,
                  alignment: Alignment.center,
                  child: const Text(
                    'Login as Patient',
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  Widget btnSubmitHospital() {
    return new Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: 350,
          height: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: loginHospital,
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Color.fromARGB(255, 253, 0, 0),
                      Color.fromARGB(255, 241, 92, 92)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  width: 400,
                  height: 100,
                  alignment: Alignment.center,
                  child: const Text(
                    'Login as Hospital',
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  Widget btnSubmitAdmin() {
    return new Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: 350,
          height: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                print(model.toMap());

                if (_conEmail.text.isEmpty) {
                  alertDialog(context, "Please Enter email");
                } else if (_conEmail.text.isEmpty) {
                  alertDialog(context, "Please Enter email");
                } else if (_conPassword.text.isEmpty) {
                  alertDialog(context, "Please Enter Password");
                } else if (_conEmail.value.text == 'admin@gmail.com' &&
                    _conPassword.value.text == '123') {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StudentsInformationPage(),
                    ),
                  );
                } else {
                  alertDialog(context, "Error: User Not Found");
                }
              },
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Ink(
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Color.fromARGB(255, 0, 81, 255),
                      Color.fromARGB(255, 64, 179, 247)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Container(
                  width: 400,
                  height: 100,
                  alignment: Alignment.center,
                  child: const Text(
                    'Login as Admin',
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
