// @dart=2.9
//pending registration
import 'dart:io';
import 'package:hospital_app/pages/Admin/edit_patient.dart';

import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/add_hospital.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

import 'edit_hospital.dart';
import 'hospitals_information.dart';

class PatientsInformationPage extends StatefulWidget {
  @override
  _PatientsInformationPageState createState() =>
      _PatientsInformationPageState();
}

class _PatientsInformationPageState extends State<PatientsInformationPage> {
  DBService dbService;

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Patients Information',
        ),
        backgroundColor: Colors.blue,
      ),
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 220,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: const Text(
                        'AN',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      'Admin Name',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'admin@gmail.com',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.groups,
              ),
              title: const Text(
                'Students Information',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => StudentsInformationPage()),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.hourglass_top,
                color: Colors.white,
              ),
              title: const Text(
                'Patients Information',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              tileColor: Colors.blue,
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => PatientsInformationPage()),
                );
              },
            ),
            SizedBox(
              height: 400,
            ),
            ListTile(
              leading: Icon(
                Icons.logout_sharp,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: _fetchData(),
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/approve.png',
                  height: 200,
                ),
              ),
            ),
            Text(
              'Patients Information',
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            FutureBuilder<List<PatientModel>>(
              future: dbService.getPatients(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<PatientModel>> students) {
                if (students.hasData) {
                  return _buildUI(students.data);
                }

                return CircularProgressIndicator();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<PatientModel> students) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(students)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(10),
    );
  }

  Widget _buildDataTable(List<PatientModel> model) {
    return Container(
      height: 500,
      child: SingleChildScrollView(
        child: DataTable(
          columns: [
            DataColumn(
              label: Text(
                "ID",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
            DataColumn(
              label: Text(
                "Name",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
            DataColumn(
              label: Text(
                "      Actions",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ],
          sortColumnIndex: 1,
          rows: model
              .map(
                (data) => DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        '${data.id}',
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    DataCell(
                      Text(
                        '${data.patientName}',
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ),
                    DataCell(
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.check_circle),
                              color: Colors.blue,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EditPatient(
                                      isEditMode: true,
                                      model: data,
                                    ),
                                  ),
                                );
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.delete),
                              color: Colors.red,
                              onPressed: () {
                                FormHelper.showMessage(
                                  context,
                                  "SQFLITE hospital_app",
                                  "Do you want to delete this record?",
                                  "Yes",
                                  () {
                                    dbService.deletePatient(data).then((value) {
                                      setState(() {
                                        Navigator.of(context).pop();
                                      });
                                    });
                                  },
                                  buttonText2: "No",
                                  isConfirmationDialog: true,
                                  onPressed2: () {
                                    Navigator.of(context).pop();
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
