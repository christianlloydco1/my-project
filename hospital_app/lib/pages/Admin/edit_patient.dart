// @dart=2.9
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class EditPatient extends StatefulWidget {
  EditPatient({Key key, this.model, this.isEditMode = true}) : super(key: key);
  PatientModel model;
  bool isEditMode;

  @override
  _EditPatientState createState() => _EditPatientState();
}

class _EditPatientState extends State<EditPatient> {
  PatientModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new PatientModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Approve Student"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormHelper.fieldLabel("Select Student Photo"),
                FormHelper.picPicker(
                  model.patientPic,
                  (file) => {
                    setState(
                      () {
                        model.patientPic = file.path;
                      },
                    )
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                FormHelper.textInputEmail(
                  context,
                  model.email,
                  (value) => {
                    this.model.email = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter Student Name.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Email should be in this format email@gmail.com';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputPassword(
                  context,
                  model.password,
                  (value) => {
                    this.model.password = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter password.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Password should have uppercase, lowercase, numeric and special character.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputName(
                  context,
                  model.patientName,
                  (value) => {
                    this.model.patientName = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Student Name.';
                    } else if (value.toString().length < 8) {
                      return 'Name should be at least 8 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputAddress(
                  context,
                  model.address,
                  (value) => {
                    this.model.address = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Address.';
                    } else if (value.toString().length < 10) {
                      return 'Address should be at least 10 characters.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputContact(
                  context,
                  model.contact,
                  (value) => {
                    this.model.contact = value,
                  },
                  isNumberInput: true,
                  onValidate: (value) {
                    String pattern = r'(^(09|\+639)\d{9}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter contact number.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Contact number should be in this format 09#########.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 30,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());

            dbService.updatePatient(model).then((value) {
              FormHelper.showMessage(
                context,
                "Update Student Record",
                "Student record updated successfully",
                "Ok",
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StudentsInformationPage(),
                    ),
                  );
                },
              );
            });
          }
        },
        child: Text('Edit Student Record'),
      ),
    );
  }
}
