// @dart=2.9
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/edit_hospital.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/add_services.dart';
import 'package:hospital_app/pages/Hospitals/edit_services.dart';
import 'package:hospital_app/pages/Hospitals/hospital_appointment_page.dart';

import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/Hospitals/reschedule_request.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';

import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class ServicesPage extends StatefulWidget {
  ServicesPage(
      {Key key, this.model, this.isEditMode = true, ServiceModel serviceModel})
      : super(key: key);
  HospitalModel model;
  ServiceModel serviceModel;
  bool isEditMode;

  @override
  _ServicesPageState createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  HospitalModel model;
  ServiceModel serviceModel;

  DBService dbService;

  // bool value = false;

  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new HospitalModel();
    serviceModel = new ServiceModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  // Widget buildCheckBox() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.science = !science;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: science,
  //         onChanged: (value) {
  //           setState(() {
  //             this.science = value;
  //           });
  //         },
  //       ),
  //       title: Text('Science'),
  //     );

  // Widget buildCheckBox2() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.physics = !physics;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: physics,
  //         onChanged: (value) {
  //           setState(() {
  //             this.physics = value;
  //           });
  //         },
  //       ),
  //       title: Text('Physics'),
  //     );

  // Widget buildCheckBox3() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.genCal = !genCal;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: genCal,
  //         onChanged: (value) {
  //           setState(() {
  //             this.genCal = value;
  //           });
  //         },
  //       ),
  //       title: Text('General Calculus'),
  //     );
  // Widget buildCheckBox4() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.java = !java;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: java,
  //         onChanged: (value) {
  //           setState(() {
  //             this.java = value;
  //           });
  //         },
  //       ),
  //       title: Text('Java'),
  //     );

  // Widget buildCheckBox5() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.appDev = !appDev;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: appDev,
  //         onChanged: (value) {
  //           setState(() {
  //             this.appDev = value;
  //           });
  //         },
  //       ),
  //       title: Text('Applications Development'),
  //     );

  String getInitials(String name) =>
      name.trim().split(' ').map((l) => l[0]).take(2).join();

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.hospitalName);
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.hospitalName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Hospital Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HospitalHome(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Services',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ServicesPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Appointments',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewHospitalAppointmentsPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.edit_calendar),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Requests',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => RescheduleRequestPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 270,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Services"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    serviceModel.hospitalId = model.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/services.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<ServiceModel>>(
              future: dbService.getServices(model.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<ServiceModel>> servicess) {
                if (servicess.hasData) {
                  return _buildUI(servicess.data);
                }

                return Text('No Servicess Added');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<ServiceModel> servicess) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(servicess)],
              ),
            ),
            SizedBox(
              height: 20,
            ),

            // ElevatedButton(
            //   style: ElevatedButton.styleFrom(
            //     primary: Colors.blue,
            //     onPrimary: Colors.white,
            //   ),
            //   onPressed: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => AddServices(
            //           serviceModel: serviceModel,
            //           hospitalModel: model,
            //         ),
            //       ),
            //     );
            //   },
            //   child: Text('Add Service'),
            // ),
            btnSubmit(),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(List<ServiceModel> serviceModel) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Service Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Price",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Actions",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: serviceModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.serviceName}',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.price}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(Icons.check_circle),
                            color: Colors.blue,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => EditServices(
                                    isEditMode: true,
                                    model: data,
                                    hospitalModel: model,
                                  ),
                                ),
                              );
                            },
                          ),
                          new IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(Icons.delete),
                            color: Colors.red,
                            onPressed: () {
                              FormHelper.showMessage(
                                context,
                                "SQFLITE hospital_app",
                                "Do you want to delete this record?",
                                "Yes",
                                () {
                                  dbService.deleteService(data).then((value) {
                                    setState(() {
                                      Navigator.of(context).pop();
                                    });
                                  });
                                },
                                buttonText2: "No",
                                isConfirmationDialog: true,
                                onPressed2: () {
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          serviceModel.hospitalId = model.id;

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddServices(
                serviceModel: serviceModel,
                hospitalModel: model,
              ),
            ),
          );
        },
        child: Text('Add Service'),
      ),
    );
  }
}
