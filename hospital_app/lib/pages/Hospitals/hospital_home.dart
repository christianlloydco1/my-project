// @dart=2.9
import 'dart:io';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/hospital_appointment_page.dart';
import 'package:hospital_app/pages/Hospitals/reschedule_request.dart';

import 'package:hospital_app/pages/Hospitals/services_page.dart';

import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class HospitalHome extends StatefulWidget {
  HospitalHome({Key key, this.model, this.isEditMode = true}) : super(key: key);
  HospitalModel model;
  bool isEditMode;

  @override
  _HospitalHomeState createState() => _HospitalHomeState();
}

class _HospitalHomeState extends State<HospitalHome> {
  HospitalModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new HospitalModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.hospitalName);
    // ignore: unnecessary_new
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.hospitalName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Hospital Profile',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HospitalHome(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Services',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ServicesPage(
                      isEditMode: true,
                      model: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Appointments',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewHospitalAppointmentsPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.edit_calendar),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Requests',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => RescheduleRequestPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 270,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      hospitalModel: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Hospital Profile"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                model.hospitalPic != null
                    ? FormHelper.picPicker(
                        model.hospitalPic,
                        (file) => {
                          setState(
                            () {
                              model.hospitalPic = file.path;
                            },
                          )
                        },
                      )
                    : FormHelper.picPicker2(
                        model.hospitalPic,
                        (file) => {
                          setState(
                            () {
                              model.hospitalPic = file.path;
                            },
                          )
                        },
                      ),

                SizedBox(
                  height: 20,
                ),

                FormHelper.textInputEmail(
                  context,
                  model.email,
                  (value) => {
                    this.model.email = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter Hospital Name.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Email should be in this format email@gmail.com';
                    }
                    return null;
                  },
                ),

                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputPassword(
                  context,
                  model.password,
                  (value) => {
                    this.model.password = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter password.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Password should have uppercase, lowercase, numeric and special character.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputName(
                  context,
                  model.hospitalName,
                  (value) => {
                    this.model.hospitalName = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Hospital Name.';
                    } else if (value.toString().length < 8) {
                      return 'Name should be at least 8 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputAddress(
                  context,
                  model.address,
                  (value) => {
                    this.model.address = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Address.';
                    } else if (value.toString().length < 10) {
                      return 'Address should be at least 10 characters.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                // FormHelper.fieldLabel("Address"),
                // FormHelper.textInput(
                //     context,
                //     model.address,
                //     (value) => {
                //           this.model.address = value,
                //         },
                //     isTextArea: true, onValidate: (value) {
                //   return null;
                // }),

                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputContact(
                  context,
                  model.contact,
                  (value) => {
                    this.model.contact = value,
                  },
                  isNumberInput: true,
                  onValidate: (value) {
                    String pattern = r'(^(09|\+639)\d{9}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter contact number.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Contact number should be in this format 09#########.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                SizedBox(
                  height: 15,
                ),

                SizedBox(
                  height: 30,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());

            dbService.updateHospital(model).then((value) {
              FormHelper.showMessage(
                context,
                "Update Student Record",
                "Student record updated successfully",
                "Ok",
                () {
                  Navigator.pop(context, true);
                },
              );
            });
          }
        },
        child: Text('Change Profile Information'),
      ),
    );
  }
}
