// @dart=2.9
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class EditServices extends StatefulWidget {
  EditServices(
      {Key key, this.model, this.hospitalModel, this.isEditMode = true})
      : super(key: key);
  ServiceModel model;
  HospitalModel hospitalModel;
  bool isEditMode;

  @override
  _EditServicesState createState() => _EditServicesState();
}

class _EditServicesState extends State<EditServices> {
  ServiceModel model;
  HospitalModel hospitalModel;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new ServiceModel();

    model = widget.model;
    hospitalModel = widget.hospitalModel;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Edit Service Information"),
      ),
      body: SingleChildScrollView(
        child: new Form(
            key: globalFormKey,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.blue,
                        Colors.white,
                      ],
                    ),
                  ),
                  child: Center(
                    child: Image.asset(
                      'assets/images/services.png',
                      height: 250,
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                _formUI(),
              ],
            )),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputServiceName(
                  context,
                  model.serviceName,
                  (value) => {
                    this.model.serviceName = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Medical Service Name.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                FormHelper.textInputServicePrice(
                  context,
                  model.price,
                  (value) => {
                    this.model.price = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter price';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 205,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());

            dbService.updateServices(model).then((value) {
              FormHelper.showMessage(
                context,
                "Update Service Record",
                "Service record updated successfully",
                "Ok",
                () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => ServicesPage(
                        isEditMode: true,
                        model: hospitalModel,
                      ),
                    ),
                  );
                },
              );
            });
          }
        },
        child: Text('Edit Information'),
      ),
    );
  }
}
