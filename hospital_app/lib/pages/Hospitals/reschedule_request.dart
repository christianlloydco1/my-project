// @dart=2.9
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/edit_hospital.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/hospital_appointment_page.dart';
import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/pages/Patients/edit_schedule.dart';
import 'package:hospital_app/pages/Patients/patients_home.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/db_helper.dart';
import 'package:hospital_app/utils/form_helper.dart';

class RescheduleRequestPage extends StatefulWidget {
  RescheduleRequestPage(
      {Key key,
      this.hospitalModel,
      this.isEditMode = true,
      this.hospitalAppointmentModel})
      : super(key: key);
  HospitalModel hospitalModel;
  GeneralAppointmentModel hospitalAppointmentModel;
  bool isEditMode;

  @override
  _RescheduleRequestPageState createState() => _RescheduleRequestPageState();
}

class _RescheduleRequestPageState extends State<RescheduleRequestPage> {
  HospitalModel hospitalModel;
  GeneralAppointmentModel hospitalAppointmentModel;
  String origTime;
  int origSchedId;
  // CountModel countModel;

  DBService dbService;

  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    hospitalModel = new HospitalModel();
    hospitalAppointmentModel = new GeneralAppointmentModel();

    if (widget.isEditMode) {
      hospitalModel = widget.hospitalModel;
      int count = 0;
    }
  }

  // Widget buildCheckBox() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.science = !science;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: science,
  //         onChanged: (value) {
  //           setState(() {
  //             this.science = value;
  //           });
  //         },
  //       ),
  //       title: Text('Science'),
  //     );

  // Widget buildCheckBox2() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.physics = !physics;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: physics,
  //         onChanged: (value) {
  //           setState(() {
  //             this.physics = value;
  //           });
  //         },
  //       ),
  //       title: Text('Physics'),
  //     );

  // Widget buildCheckBox3() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.genCal = !genCal;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: genCal,
  //         onChanged: (value) {
  //           setState(() {
  //             this.genCal = value;
  //           });
  //         },
  //       ),
  //       title: Text('General Calculus'),
  //     );
  // Widget buildCheckBox4() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.java = !java;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: java,
  //         onChanged: (value) {
  //           setState(() {
  //             this.java = value;
  //           });
  //         },
  //       ),
  //       title: Text('Java'),
  //     );

  // Widget buildCheckBox5() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.appDev = !appDev;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: appDev,
  //         onChanged: (value) {
  //           setState(() {
  //             this.appDev = value;
  //           });
  //         },
  //       ),
  //       title: Text('Applications Development'),
  //     );

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(hospitalModel.hospitalName);
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${hospitalModel.hospitalName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${hospitalModel.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Hospital Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HospitalHome(
                      isEditMode: true,
                      model: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Services',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ServicesPage(
                      isEditMode: true,
                      model: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Appointments',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewHospitalAppointmentsPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.edit_calendar,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: Text(
                'Reschedule Requests',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => RescheduleRequestPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 270,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Reschedule Request"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    hospitalAppointmentModel.hospitalId = hospitalModel.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/appoint.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<GeneralAppointmentModel>>(
              future: DB.queryRescheduleAppointments(hospitalModel.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GeneralAppointmentModel>> appointments) {
                if (appointments.hasData) {
                  return _buildUI(appointments.data);
                }

                return Text('No Subjects Enrolled');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<GeneralAppointmentModel> appointments) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [_buildDataTable(appointments)],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(
      List<GeneralAppointmentModel> hospitalAppointmentModel) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Patient Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Service Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Price",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Original Date",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Requested Date",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Original Time",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Requested Time",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Status",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "     Actiions",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: hospitalAppointmentModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.patientName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.serviceName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.price}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.scheduleDateOld}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.scheduleDateNew}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.timeRangeOld}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.timeRangeNew}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.rescheduleStatus}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.thumb_up),
                          color: Colors.blue,
                          onPressed: () {
                            FormHelper.showMessage(
                              context,
                              "Approve Reschedule",
                              "Are you sure that you want to approve rescheduling this appointment?",
                              "Yes",
                              () {
                                Navigator.of(context).pop();
                                print(data.toMap());
                                origSchedId = data.scheduleId;
                                print(origSchedId);
                                origTime = data.timeRange;
                                var newId = data.scheduleIdNew;

                                print('${data.timeRange}');
                                dbService.updateRescheduleTimeApproved(
                                    data, data.scheduleId);

                                data.scheduleDate = data.scheduleDateOld;
                                print('${data.timeRange}');

                                dbService
                                    .updateScheduleTimeApproved(
                                        data, data.scheduleId)
                                    .then((value) {
                                  FormHelper.showMessage(
                                    context,
                                    "Approve Reschedule",
                                    "Appointment rescheduled successfully.",
                                    "Ok",
                                    () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ViewHospitalAppointmentsPage(
                                            hospitalModel: hospitalModel,
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                });

                                dbService.updateScheduleTimeApproved2(data);

                                dbService.updateAppointmentOld(
                                    data, data.scheduleIdNew);
                              },
                              buttonText2: "No",
                              isConfirmationDialog: true,
                              onPressed2: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        ),
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.thumb_down),
                          color: Colors.red,
                          onPressed: () {
                            FormHelper.showMessage(
                              context,
                              "Deny Reschedule",
                              "Are you sure that you want to deny rescheduling this appointment?",
                              "Yes",
                              () {
                                Navigator.of(context).pop();
                                print(data.toMap());
                                dbService.updateRescheduleTimeDenied(
                                    data, data.scheduleId);
                                data.status = 'Denied';

                                dbService
                                    .updateScheduleTimeDenied(
                                        data, data.scheduleIdOld)
                                    .then((value) {
                                  FormHelper.showMessage(
                                    context,
                                    "Deny Reschedule",
                                    "Appointment reschedule successfully denied.",
                                    "Ok",
                                    () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              RescheduleRequestPage(
                                            hospitalModel: hospitalModel,
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                });

                                // dbService.updateScheduleTimeDenied2(
                                //     data, data.scheduleId);

                                data.scheduleId = data.scheduleIdOld;

                                // dbService.updateAppointmentOld(
                                //     data, data.scheduleIdNew);
                              },
                              buttonText2: "No",
                              isConfirmationDialog: true,
                              onPressed2: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
