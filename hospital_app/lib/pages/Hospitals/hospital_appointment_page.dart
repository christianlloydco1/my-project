// @dart=2.9
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/edit_hospital.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/Hospitals/reschedule_request.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/pages/Patients/edit_schedule.dart';
import 'package:hospital_app/pages/Patients/patients_home.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/db_helper.dart';
import 'package:hospital_app/utils/form_helper.dart';

class ViewHospitalAppointmentsPage extends StatefulWidget {
  ViewHospitalAppointmentsPage(
      {Key key,
      this.hospitalModel,
      this.isEditMode = true,
      this.patientAppointmentModel})
      : super(key: key);
  HospitalModel hospitalModel;
  GeneralAppointmentModel patientAppointmentModel;
  bool isEditMode;

  @override
  _ViewHospitalAppointmentsPageState createState() =>
      _ViewHospitalAppointmentsPageState();
}

class _ViewHospitalAppointmentsPageState
    extends State<ViewHospitalAppointmentsPage> {
  HospitalModel hospitalModel;
  GeneralAppointmentModel patientAppointmentModel;

  DBService dbService;

  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    hospitalModel = new HospitalModel();
    patientAppointmentModel = new GeneralAppointmentModel();

    if (widget.isEditMode) {
      hospitalModel = widget.hospitalModel;
    }
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(hospitalModel.hospitalName);
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${hospitalModel.hospitalName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${hospitalModel.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Hospital Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HospitalHome(
                      isEditMode: true,
                      model: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.receipt_long,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Services',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ServicesPage(
                      isEditMode: true,
                      model: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Appointments',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewHospitalAppointmentsPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.edit_calendar),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Requests',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => RescheduleRequestPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 270,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      hospitalModel: hospitalModel,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Appointments"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    patientAppointmentModel.hospitalId = hospitalModel.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/appoint.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<GeneralAppointmentModel>>(
              future: DB.queryHospitalAppointments(
                  'appointments', hospitalModel.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GeneralAppointmentModel>> subjects) {
                if (subjects.hasData) {
                  return _buildUI(subjects.data);
                }

                return Text('No Subjects Enrolled');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<GeneralAppointmentModel> subjects) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [_buildDataTable(subjects)],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(
      List<GeneralAppointmentModel> patientAppointmentModel) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Hospital Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Service Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Price",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Date",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Time",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Status",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: patientAppointmentModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.hospitalName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.serviceName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.price}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.scheduleDate}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.timeRange}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.status}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
