// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class AddServices extends StatefulWidget {
  AddServices(
      {Key key, this.serviceModel, this.hospitalModel, this.isEditMode = false})
      : super(key: key);
  ServiceModel serviceModel;
  HospitalModel hospitalModel;
  bool isEditMode;

  @override
  _AddServicesState createState() => _AddServicesState();
}

class _AddServicesState extends State<AddServices> {
  ServiceModel serviceModel;
  HospitalModel hospitalModel;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    serviceModel = new ServiceModel();
    hospitalModel = new HospitalModel();

    serviceModel = widget.serviceModel;
    hospitalModel = widget.hospitalModel;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Add Service/s"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.blue,
                  Colors.white,
                ],
              ),
            ),
            child: Center(
              child: Image.asset(
                'assets/images/services.png',
                height: 250,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Container(
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    FormHelper.textInputServiceName(
                      context,
                      serviceModel.serviceName,
                      (value) => {
                        this.serviceModel.serviceName = value,
                      },
                      onValidate: (value) {
                        if (value.toString().isEmpty) {
                          return 'Please enter Medical Service Name.';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FormHelper.textInputServicePrice(
                      context,
                      serviceModel.price,
                      (value) => {
                        this.serviceModel.price = value,
                      },
                      onValidate: (value) {
                        if (value.toString().isEmpty) {
                          return 'Please enter price';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    btnSubmit(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(serviceModel.toMap());

            serviceModel.hospitalId = hospitalModel.id;

            dbService.addServices(serviceModel).then((value) {
              FormHelper.showMessage(
                context,
                "Add Service",
                "Service added successfully",
                "Ok",
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ServicesPage(
                        model: hospitalModel,
                      ),
                    ),
                  );
                },
              );
            });
          }
        },
        child: Text('Add Service'),
      ),
    );
  }
}
