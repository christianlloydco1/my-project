// @dart=2.9
import 'dart:io';

import 'package:hospital_app/pages/Admin/edit_hospital.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/add_services.dart';
import 'package:hospital_app/pages/Hospitals/hospital_home.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/pages/Patients/book_appointment.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class HospitalPage extends StatefulWidget {
  HospitalPage({
    Key key,
    this.model,
    this.patientModel,
    this.isEditMode = true,
    ServiceModel serviceModel,
  }) : super(key: key);
  HospitalModel model;
  ServiceModel serviceModel;
  PatientModel patientModel;
  bool isEditMode;

  @override
  _HospitalPageState createState() => _HospitalPageState();
}

class _HospitalPageState extends State<HospitalPage> {
  HospitalModel model;
  ServiceModel serviceModel;
  PatientModel patientModel;

  DBService dbService;

  // bool value = false;

  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new HospitalModel();
    serviceModel = new ServiceModel();
    patientModel = new PatientModel();

    model = widget.model;
    patientModel = widget.patientModel;
  }

  String getInitials(String name) =>
      name.trim().split(' ').map((l) => l[0]).take(2).join();

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.hospitalName);
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Hospital Details"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    serviceModel.hospitalId = model.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text('${model.hospitalName}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  )),
            ),
            Text('${model.address}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              height: 20,
            ),
            Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15),
                child: model.hospitalPic.contains('https')
                    ? Image.network(
                        model.hospitalPic,
                        fit: BoxFit.cover,
                      )
                    : Image.file(
                        File(model.hospitalPic),
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                      )),
            FutureBuilder<List<ServiceModel>>(
              future: dbService.getServices(model.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<ServiceModel>> hospitals) {
                if (hospitals.hasData) {
                  return _buildUI(hospitals.data);
                }

                return Text('No Services');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<ServiceModel> hospitals) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Text('Medical Services',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              height: 320,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(hospitals)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(List<ServiceModel> serviceModelList) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          width: 500,
          child: DataTable(
            columns: [
              DataColumn(
                label: Text(
                  "Service Name",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  "Price",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  "         Action",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
            ],
            sortColumnIndex: 1,
            rows: serviceModelList
                .map(
                  (data) => DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          '${data.serviceName}',
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                      DataCell(
                        Text(
                          '${data.price}',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      DataCell(ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          onPrimary: Colors.white,
                        ),
                        onPressed: () {
                          serviceModel.hospitalId = model.id;

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BookAppointment(
                                hospitalModel: model,
                                serviceModel: data,
                                patientModel: patientModel,
                              ),
                            ),
                          );
                        },
                        child: Text('Book Appointment'),
                      )),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.blue,
        onPrimary: Colors.white,
      ),
      onPressed: () {
        serviceModel.hospitalId = model.id;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BookAppointment(),
          ),
        );
      },
      child: Text('Book Appointment'),
    );
  }
}
