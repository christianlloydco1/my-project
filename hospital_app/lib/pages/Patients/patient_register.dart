// @dart=2.9
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class RegisterPatient extends StatefulWidget {
  RegisterPatient({Key key, this.model, this.isEditMode = false})
      : super(key: key);
  PatientModel model;
  bool isEditMode;

  @override
  _RegisterPatientState createState() => _RegisterPatientState();
}

class _RegisterPatientState extends State<RegisterPatient> {
  PatientModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new PatientModel();

    if (widget.isEditMode) {
      model = widget.model;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Registration'),
        backgroundColor: Colors.blue,
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    TextEditingController confrimPassword = TextEditingController();
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Align(
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50)),
                    child: SingleChildScrollView(
                      child: Container(
                        width: 450,
                        height: 300,
                        padding: EdgeInsets.only(bottom: 20),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.blue,
                              Colors.white,
                            ],
                          ),
                        ),
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: Image.asset(
                            'assets/images/register.png',
                          ),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    'Register',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ],
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    children: [
                      FormHelper.fieldLabel(
                        "Select Student Photo",
                      ),
                      FormHelper.picPicker2(
                        model.patientPic,
                        (file) => {
                          setState(
                            () {
                              model.patientPic = file.path;
                            },
                          )
                        },
                      ),

                      SizedBox(
                        height: 20,
                      ),

                      FormHelper.textInputEmail(
                        context,
                        model.email,
                        (value) => {
                          this.model.email = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter Student Name.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Email should be in this format email@gmail.com';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputPassword(
                        context,
                        model.password,
                        (value) => {
                          this.model.password = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter password.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Password should have uppercase, lowercase, numeric and special character.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      TextFormField(
                        controller: confrimPassword,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue),
                          ),
                          prefixIcon: Icon(Icons.lock),
                          hintText: 'Confirm Password',
                          labelText: 'Confirm Password',
                          fillColor: Colors.grey[200],
                          filled: true,
                        ),
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please confirm your password.';
                          } else if (confrimPassword.text != model.password) {
                            return 'Password mismatch.';
                          } else {
                            return null;
                          }
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputName(
                        context,
                        model.patientName,
                        (value) => {
                          this.model.patientName = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Student Name.';
                          } else if (value.toString().length < 8) {
                            return 'Name should be at least 8 characters';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputAddress(
                        context,
                        model.address,
                        (value) => {
                          this.model.address = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Address.';
                          } else if (value.toString().length < 10) {
                            return 'Address should be at least 10 characters.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      FormHelper.textInputBday(
                        context,
                        model.bDay,
                        (value) => {
                          this.model.bDay = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$)';

                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter birthday.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Birthday should be in this format MM/DD/YYYY.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      FormHelper.textInputGender(
                        context,
                        model.gender,
                        (value) => {
                          this.model.gender = value,
                        },
                        onValidate: (value) {
                          String pattern =
                              r'(^(?:m|M|male|Male|f|F|female|Female)$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter gender.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Gender should be Male or Female.';
                          }

                          return null;
                        },
                      ),

                      // FormHelper.fieldLabel("Address"),
                      // FormHelper.textInput(
                      //     context,
                      //     model.address,
                      //     (value) => {
                      //           this.model.address = value,
                      //         },
                      //     isTextArea: true, onValidate: (value) {
                      //   return null;
                      // }),

                      SizedBox(
                        height: 10,
                      ),

                      FormHelper.textInputContact(
                        context,
                        model.contact,
                        (value) => {
                          this.model.contact = value,
                        },
                        isNumberInput: true,
                        onValidate: (value) {
                          String pattern = r'(^(09|\+639)\d{9}$)';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter contact number.';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Contact number should be in this format 09#########.';
                          }
                          return null;
                        },
                      ),

                      SizedBox(
                        height: 10,
                      ),

                      btnSubmit(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());
            if (widget.isEditMode) {
              dbService.updatePatient(model).then((value) {
                FormHelper.showMessage(
                  context,
                  "Patient Record",
                  "Patient record updated successfully",
                  "Ok",
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PatientsInformationPage(),
                      ),
                    );
                  },
                );
              });
            } else {
              dbService.addPatient(model).then((value) {
                FormHelper.showMessage(
                  context,
                  "Patient Registration",
                  "Patient registered successfully",
                  "Ok",
                  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ),
                    );
                  },
                );
              });
            }
          }
        },
        child: Text('Register'),
      ),
    );
  }
}
