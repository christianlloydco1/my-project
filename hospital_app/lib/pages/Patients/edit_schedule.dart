// @dart=2.9
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';
import 'package:intl/intl.dart';

class EditSchedule extends StatefulWidget {
  EditSchedule(
      {Key key,
      this.appointmentModel,
      this.patientModel,
      this.isEditMode = true})
      : super(key: key);
  GeneralAppointmentModel appointmentModel;
  PatientModel patientModel;
  bool isEditMode;

  @override
  _EditScheduleState createState() => _EditScheduleState();
}

class _EditScheduleState extends State<EditSchedule> {
  GeneralAppointmentModel appointmentModel;
  PatientModel patientModel;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  final _conDate = TextEditingController();
  final _conDateNew = TextEditingController();
  final _conTime = TextEditingController();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    appointmentModel = new GeneralAppointmentModel();
    patientModel = new PatientModel();

    if (widget.isEditMode) {
      appointmentModel = widget.appointmentModel;
      patientModel = widget.patientModel;
      _conDate.text = appointmentModel.scheduleDate;

      var currentDate = DateTime.now();
      String formattedDateNow = DateFormat('M/d/yyyy').format(currentDate);

      _conDateNew.text = formattedDateNow.toString();
      _conTime.text = appointmentModel.timeRange;
    }
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/appoint.png',
                  height: 250,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Container(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      FormHelper.textInputServiceName2(
                        context,
                        appointmentModel.serviceName,
                        (value) => {
                          this.appointmentModel.serviceName = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Medical Service Name.';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      FormHelper.textInputServicePrice2(
                        context,
                        appointmentModel.price,
                        (value) => {
                          this.appointmentModel.price = value,
                        },
                        onValidate: (value) {
                          String pattern = r'^\d+(,\d{1,2})?$';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter price';
                          } else if (value.toString == "") {
                            return 'Please enter price';
                          } else if (!regExp.hasMatch(value)) {
                            print('value : ${value.toString()}');
                            return 'Please enter numerical value';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                          child: TextField(
                        enabled: false,
                        controller:
                            _conDate, //editing controller of this TextField
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue),
                          ),
                          prefixIcon: Icon(Icons.calendar_today),
                          hintText: 'M/d/yyyy',
                          //icon of text field
                          labelText: "Old Date",
                          fillColor: Colors.grey[200],
                          filled: true,
                        ),

                        readOnly:
                            true, //set it true, so that user will not able to edit text
                        onTap: () async {
                          DateTime pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime
                                  .now(), //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('M/d/yyyy').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              _conDate.text =
                                  formattedDate; //set output date to TextField value.
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                      )),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                          child: TextField(
                        controller:
                            _conDateNew, //editing controller of this TextField
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue),
                          ),
                          prefixIcon: Icon(Icons.calendar_today),
                          hintText: 'Enter new date',
                          //icon of text field
                          labelText: "New Date",
                          fillColor: Colors.grey[200],
                          filled: true,
                        ),

                        readOnly:
                            true, //set it true, so that user will not able to edit text
                        onTap: () async {
                          DateTime pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime
                                  .now(), //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('M/d/yyyy').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              _conDateNew.text =
                                  formattedDate; //set output date to TextField value.
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                      )),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                          child: TextField(
                        enabled: false,
                        controller:
                            _conTime, //editing controller of this TextField
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue),
                          ),
                          prefixIcon: Icon(Icons.calendar_today),
                          hintText: 'hh:mm AM',
                          //icon of text field
                          labelText: "Old Time",
                          fillColor: Colors.grey[200],
                          filled: true,
                        ),

                        readOnly:
                            true, //set it true, so that user will not able to edit text
                        onTap: () async {
                          DateTime pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime
                                  .now(), //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('M/d/yyyy').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              _conDate.text =
                                  formattedDate; //set output date to TextField value.
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                      )),
                    ],
                  ),
                ),
              ),
            ),
            FutureBuilder<List<GeneralAppointmentModel>>(
              future: dbService.getGeneralAppointment(
                  appointmentModel.hospitalId, _conDateNew.text),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GeneralAppointmentModel>> appointments) {
                if (appointments.hasData) {
                  return _buildUI(appointments.data);
                }

                return Text('No Services');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<GeneralAppointmentModel> appointments) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Text('Schedules',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              height: 500,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(appointments)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(
      List<GeneralAppointmentModel> generalAppointmentModelList) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          width: 370,
          child: DataTable(
            columns: [
              DataColumn(
                label: Text(
                  "Time",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  "         Action",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
            ],
            sortColumnIndex: 1,
            rows: generalAppointmentModelList
                .map(
                  (data) => DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          '${data.timeRange}',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      DataCell(ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          onPrimary: Colors.white,
                        ),
                        onPressed: () {
                          var originalSched = appointmentModel.scheduleId;
                          print('orig Sched id: $originalSched');

                          String origTime = appointmentModel.timeRange;

                          appointmentModel.patientId = patientModel.id;

                          appointmentModel.scheduleDate = _conDate.text;
                          appointmentModel.scheduleDateOld = _conDate.text;
                          appointmentModel.scheduleDateNew = _conDateNew.text;

                          appointmentModel.timeRangeOld = _conTime.text;
                          appointmentModel.timeRangeNew = data.timeRange;
                          appointmentModel.status = 'Waiting for Approval';
                          appointmentModel.rescheduleStatus =
                              'Waiting for Approval';
                          appointmentModel.scheduleIdOld = originalSched;
                          appointmentModel.scheduleIdNew = data.scheduleId;
                          // appointmentModel.time = _conTime.text;
                          // appointmentModel.status = 'Scheduled';
                          appointmentModel.scheduleId =
                              appointmentModel.scheduleIdOld;
                          print('Sched old: ${appointmentModel.scheduleIdOld}');
                          dbService.updateScheduleTimeOld(appointmentModel);
                          print(appointmentModel.toMapAppointments());
                          print(
                              'Sched Id:${appointmentModel.scheduleId}, timeRange: ${appointmentModel.timeRange}, status: ${appointmentModel.status}, ${appointmentModel.hospitalId}');

                          dbService
                              .addRescheduleAppointment(appointmentModel)
                              .then((value) {
                            appointmentModel.timeRange = origTime;
                            appointmentModel.status = 'Open';
                            appointmentModel.scheduleId = originalSched;

                            // dbService.updateScheduleTime(
                            //     appointmentModel, appointmentModel.scheduleId);

                            FormHelper.showMessage(
                              context,
                              "Reschedule Requested",
                              "Please wait until the hospital approved your reschedule request",
                              "Ok",
                              () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ViewAppointmentsPage(
                                      patientModel: patientModel,
                                    ),
                                  ),
                                );
                              },
                            );
                          });

                          // appointmentModel.scheduleId = data.scheduleId;

                          // dbService.updateAppointment(
                          //     appointmentModel, originalSched);
                        },
                        child: Text('Reschedule'),
                      )),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Reschedule"),
      ),
      body: Column(
        children: [
          new Form(
            key: globalFormKey,
            child: SingleChildScrollView(
                child: Container(height: 720, child: _fetchData())),
          ),
        ],
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
