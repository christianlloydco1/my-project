// @dart=2.9
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/Comm/genTextFormField.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Hospitals/services_page.dart';
import 'package:hospital_app/pages/Patients/hospital_page.dart';
import 'package:hospital_app/pages/Patients/receipt.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';
import 'package:intl/intl.dart';

class BookAppointment extends StatefulWidget {
  BookAppointment(
      {Key key,
      this.serviceModel,
      this.hospitalModel,
      this.patientModel,
      this.isEditMode = false})
      : super(key: key);
  ServiceModel serviceModel;
  HospitalModel hospitalModel;
  PatientModel patientModel;
  GeneralAppointmentModel appointmentModel;
  bool isEditMode;

  @override
  _BookAppointmentState createState() => _BookAppointmentState();
}

class _BookAppointmentState extends State<BookAppointment> {
  ServiceModel serviceModel;
  HospitalModel hospitalModel;
  PatientModel patientModel;
  GeneralAppointmentModel appointmentModel;
  DBService dbService;

  final _conDate = TextEditingController();
  var currentDate;
  final _conTime = TextEditingController();

  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    currentDate = DateTime.now();
    String formattedDateNow = DateFormat('M/d/yyyy').format(currentDate);

    _conDate.text = formattedDateNow.toString();
    _conTime.text = '';
    dbService = new DBService();
    serviceModel = new ServiceModel();
    hospitalModel = new HospitalModel();
    patientModel = new PatientModel();
    appointmentModel = new GeneralAppointmentModel();
    String serviceName;
    serviceModel = widget.serviceModel;
    hospitalModel = widget.hospitalModel;
    patientModel = widget.patientModel;
  }

  Widget _fetchData() {
    serviceModel.hospitalId = hospitalModel.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/appoint.png',
                  height: 250,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Container(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      FormHelper.textInputServiceName2(
                        context,
                        serviceModel.serviceName,
                        (value) => {
                          this.serviceModel.serviceName = value,
                        },
                        onValidate: (value) {
                          if (value.toString().isEmpty) {
                            return 'Please enter Medical Service Name.';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      FormHelper.textInputServicePrice2(
                        context,
                        serviceModel.price,
                        (value) => {
                          this.serviceModel.price = value,
                        },
                        onValidate: (value) {
                          String pattern = r'^\d+(,\d{1,2})?$';
                          RegExp regExp = new RegExp(pattern);
                          if (value.toString().isEmpty) {
                            return 'Please enter price';
                          } else if (value.toString == "") {
                            return 'Please enter price';
                          } else if (!regExp.hasMatch(value)) {
                            print('value : ${value.toString()}');
                            return 'Please enter numerical value';
                          }
                          return null;
                        },
                      ),
                      Container(
                          height: 100,
                          child: Center(
                              child: TextField(
                            controller:
                                _conDate, //editing controller of this TextField
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.transparent),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue),
                              ),
                              prefixIcon: Icon(Icons.calendar_today),
                              hintText: '500',
                              //icon of text field
                              labelText: "Enter Date",
                              fillColor: Colors.grey[200],
                              filled: true,
                            ),

                            readOnly:
                                true, //set it true, so that user will not able to edit text
                            onTap: () async {
                              DateTime pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime
                                      .now(), //DateTime.now() - not to allow to choose before today.
                                  lastDate: DateTime(2101));

                              if (pickedDate != null) {
                                print(
                                    pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                                String formattedDate =
                                    DateFormat('M/d/yyyy').format(pickedDate);
                                print(
                                    formattedDate); //formatted date output using intl package =>  2021-03-16
                                //you can implement different kind of Date Format here according to your requirement

                                setState(() {
                                  _conDate.text =
                                      formattedDate; //set output date to TextField value.
                                });
                              } else {
                                print("Date is not selected");
                              }
                            },
                          ))),

                      // btnSubmit(),
                    ],
                  ),
                ),
              ),
            ),
            FutureBuilder<List<GeneralAppointmentModel>>(
              future: dbService.getGeneralAppointment(
                  hospitalModel.id, _conDate.text),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GeneralAppointmentModel>> appointments) {
                if (appointments.hasData) {
                  return _buildUI(appointments.data);
                }

                return Text('No Services');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<GeneralAppointmentModel> appointments) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Text('Schedules',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )),
            SizedBox(
              height: 500,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(appointments)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(
      List<GeneralAppointmentModel> generalAppointmentModelList) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          width: 390,
          child: DataTable(
            columns: [
              DataColumn(
                label: Text(
                  "Time",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  "         Action",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
            ],
            sortColumnIndex: 1,
            rows: generalAppointmentModelList
                .map(
                  (data) => DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          '${data.timeRange}',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      DataCell(ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          onPrimary: Colors.white,
                        ),
                        onPressed: () {
                          serviceModel.hospitalId = hospitalModel.id;
                          appointmentModel.patientId = patientModel.id;
                          appointmentModel.hospitalId = hospitalModel.id;
                          appointmentModel.serviceName =
                              serviceModel.serviceName;
                          appointmentModel.price = serviceModel.price;
                          appointmentModel.scheduleDate = _conDate.text;
                          appointmentModel.timeRange = data.timeRange;
                          appointmentModel.status = 'Scheduled';
                          appointmentModel.scheduleId = data.scheduleId;

                          print(appointmentModel.toMapAppointments());
                          print(
                              'Sched Id:${appointmentModel.scheduleId}, timeRange: ${appointmentModel.timeRange}, status: ${appointmentModel.status}, ${appointmentModel.hospitalId}');
                          dbService.updateScheduleTime(
                              appointmentModel, appointmentModel.scheduleId);

                          dbService
                              .addGeneralAppointment(appointmentModel)
                              .then((value) {
                            print(appointmentModel.toMap());
                            print(appointmentModel);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ReceiptPage(
                                  hospitalModel: hospitalModel,
                                  serviceModel: serviceModel,
                                  patientModel: patientModel,
                                  appointmentModel: appointmentModel,
                                  isEditMode: true,
                                ),
                              ),
                            );
                          });
                        },
                        child: Text('Book Appointment'),
                      )),
                    ],
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String serviceName;
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Book Appointment"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          serviceModel.hospitalId = hospitalModel.id;
          appointmentModel.patientId = patientModel.id;
          appointmentModel.hospitalId = hospitalModel.id;
          appointmentModel.serviceName = serviceModel.serviceName;
          appointmentModel.price = serviceModel.price;
          // appointmentModel.date = _conDate.text;
          // appointmentModel.time = _conTime.text;
          // appointmentModel.status = 'Scheduled';
          print(appointmentModel.toMap());

          dbService.addGeneralAppointment(appointmentModel).then((value) {
            print(appointmentModel.toMap());
            print(appointmentModel);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ReceiptPage(
                  hospitalModel: hospitalModel,
                  serviceModel: serviceModel,
                  patientModel: patientModel,
                  appointmentModel: appointmentModel,
                  isEditMode: true,
                ),
              ),
            );
          });
        },
        child: Text('Book Appointment'),
      ),
    );
  }
}
