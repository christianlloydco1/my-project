// @dart=2.9

import 'dart:ffi';
import 'dart:io';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/add_hospital.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class HospitalInformationPage extends StatefulWidget {
  @override
  _HospitalInformationPageState createState() =>
      _HospitalInformationPageState();
}

class _HospitalInformationPageState extends State<HospitalInformationPage> {
  DBService dbService;

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Hospitals Information',
        ),
        backgroundColor: Colors.blue,
      ),
      body: _fetchData(),
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 143, 236, 146),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 220,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: const Text(
                        'AN',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      'Admin Name',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'admin@gmail.com',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.groups,
                color: Colors.white,
              ),
              textColor: Colors.white,
              title: const Text(
                'Students Information',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              tileColor: Colors.blue,
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => HospitalInformationPage()),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.hourglass_top,
              ),
              title: const Text(
                'Patients Information',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (context) => PatientsInformationPage()),
                );
              },
            ),
            SizedBox(
              height: 310,
            ),
            ListTile(
              leading: Icon(
                Icons.logout_sharp,
              ),
              tileColor: Color.fromARGB(255, 143, 236, 146),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/studInfo.png',
                  height: 250,
                ),
              ),
            ),
            FloatingActionButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SearchHospitalPage(),
                  ),
                );
              },
              child: Icon(Icons.search),
            ),
            FutureBuilder<List<HospitalModel>>(
              future: dbService.getHospitals(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<HospitalModel>> students) {
                if (students.hasData) {
                  return _buildUI(students.data);
                }

                return CircularProgressIndicator();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<HospitalModel> students) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [_buildDataTable(students)],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(10),
    );
  }

  Widget _buildDataTable(List<HospitalModel> model) {
    return DataTable(
      columns: [
        DataColumn(
          label: Text(
            "ID",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            "Name",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            "      Actions",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ],
      sortColumnIndex: 1,
      rows: model
          .map(
            (data) => DataRow(
              cells: <DataCell>[
                DataCell(
                  Text(
                    '${data.id}',
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                DataCell(
                  Text(
                    '${data.hospitalName}',
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
                DataCell(
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.check_circle),
                          color: Colors.blue,
                          onPressed: () {},
                        ),
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.delete),
                          color: Colors.red,
                          onPressed: () {
                            FormHelper.showMessage(
                              context,
                              "SQFLITE hospital_app",
                              "Do you want to delete this record?",
                              "Yes",
                              () {
                                dbService.deleteStudent(data).then((value) {
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                });
                              },
                              buttonText2: "No",
                              isConfirmationDialog: true,
                              onPressed2: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
          .toList(),
    );
  }
}
