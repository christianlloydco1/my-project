// @dart=2.9
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/edit_hospital.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Patients/edit_schedule.dart';
import 'package:hospital_app/pages/Patients/patients_home.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/Patients/reschedule_request.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/db_helper.dart';
import 'package:hospital_app/utils/form_helper.dart';

class ViewAppointmentsPage extends StatefulWidget {
  ViewAppointmentsPage(
      {Key key,
      this.patientModel,
      this.isEditMode = true,
      this.patientAppointmentModel})
      : super(key: key);
  PatientModel patientModel;
  GeneralAppointmentModel patientAppointmentModel;
  bool isEditMode;

  @override
  _ViewAppointmentsPageState createState() => _ViewAppointmentsPageState();
}

class _ViewAppointmentsPageState extends State<ViewAppointmentsPage> {
  PatientModel model;
  GeneralAppointmentModel patientAppointmentModel;

  DBService dbService;

  List selectedData = [];
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new PatientModel();
    patientAppointmentModel = new GeneralAppointmentModel();

    if (widget.isEditMode) {
      model = widget.patientModel;
    }
  }

  // Widget buildCheckBox() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.science = !science;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: science,
  //         onChanged: (value) {
  //           setState(() {
  //             this.science = value;
  //           });
  //         },
  //       ),
  //       title: Text('Science'),
  //     );

  // Widget buildCheckBox2() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.physics = !physics;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: physics,
  //         onChanged: (value) {
  //           setState(() {
  //             this.physics = value;
  //           });
  //         },
  //       ),
  //       title: Text('Physics'),
  //     );

  // Widget buildCheckBox3() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.genCal = !genCal;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: genCal,
  //         onChanged: (value) {
  //           setState(() {
  //             this.genCal = value;
  //           });
  //         },
  //       ),
  //       title: Text('General Calculus'),
  //     );
  // Widget buildCheckBox4() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.java = !java;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: java,
  //         onChanged: (value) {
  //           setState(() {
  //             this.java = value;
  //           });
  //         },
  //       ),
  //       title: Text('Java'),
  //     );

  // Widget buildCheckBox5() => ListTile(
  //       onTap: () {
  //         setState(() {
  //           this.appDev = !appDev;
  //         });
  //       },
  //       trailing: Checkbox(
  //         value: appDev,
  //         onChanged: (value) {
  //           setState(() {
  //             this.appDev = value;
  //           });
  //         },
  //       ),
  //       title: Text('Applications Development'),
  //     );

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.patientName);
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.patientName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Patient Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PatientHome(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.solidHospital,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Hospitals',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SearchHospitalPage(
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Appointments',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewAppointmentsPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.edit_calendar,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Request',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => PatientRescheduleRequestPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 250,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Appointments"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _fetchData(),
      ),
    );
  }

  Widget _fetchData() {
    patientAppointmentModel.hospitalId = model.id;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
              child: Center(
                child: Image.asset(
                  'assets/images/appoint.png',
                  height: 250,
                ),
              ),
            ),
            FutureBuilder<List<GeneralAppointmentModel>>(
              future: DB.queryPatientAppointments('appointments', model.id),
              builder: (BuildContext context,
                  AsyncSnapshot<List<GeneralAppointmentModel>> appointments) {
                if (appointments.hasData) {
                  return _buildUI(appointments.data);
                }

                return Text('Patient does not have any appointment');
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUI(List<GeneralAppointmentModel> appointments) {
    List<Widget> widgets = new List<Widget>();

    widgets.add(
      new Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            SizedBox(
              height: 535,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [_buildDataTable(appointments)],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );

    // widgets.add(
    //   Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: [_buildDataTable(students)],
    //   ),
    // );

    return Padding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
      padding: EdgeInsets.all(0),
    );
  }

  Widget _buildDataTable(
      List<GeneralAppointmentModel> patientAppointmentModel) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
            label: Text(
              "Hospital Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Service Name",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Price",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Date",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Time",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "Status",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              "     Actiions",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w900,
              ),
            ),
          ),
        ],
        sortColumnIndex: 1,
        rows: patientAppointmentModel
            .map(
              (data) => DataRow(
                cells: <DataCell>[
                  DataCell(
                    Text(
                      '${data.hospitalName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.serviceName}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.price}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.scheduleDate}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.timeRange}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Text(
                      '${data.status}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  DataCell(
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.edit_calendar_rounded),
                          color: Colors.blue,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => EditSchedule(
                                  isEditMode: true,
                                  appointmentModel: data,
                                  patientModel: model,
                                ),
                              ),
                            );
                          },
                        ),
                        new IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.delete),
                          color: Colors.red,
                          onPressed: () {
                            FormHelper.showMessage(
                              context,
                              "Cancel Schedule",
                              "Are you sure that you want to cancel this schedule?",
                              "Yes",
                              () {
                                data.status = 'Cancelled';
                                Navigator.of(context).pop();
                                print(data.toMap());

                                dbService
                                    .updateScheduleTime(data, data.scheduleId)
                                    .then((value) {
                                  FormHelper.showMessage(
                                    context,
                                    "Cancell Schedule",
                                    "Schedule successfully cancelled",
                                    "Ok",
                                    () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ViewAppointmentsPage(
                                            patientModel: model,
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                });
                              },
                              buttonText2: "No",
                              isConfirmationDialog: true,
                              onPressed2: () {
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
