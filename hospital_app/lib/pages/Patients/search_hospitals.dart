// @dart=2.9

import 'dart:async';
import 'dart:io';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Patients/hospital_page.dart';
import 'package:hospital_app/pages/Patients/patients_home.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/Patients/reschedule_request.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/add_hospital.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/db_helper.dart';
import 'package:hospital_app/utils/form_helper.dart';
import 'package:hospital_app/widget/search_price.dart';
import 'package:hospital_app/widget/search_service_widget.dart';
import 'package:hospital_app/widget/search_widget.dart';

class SearchHospitalPage extends StatefulWidget {
  SearchHospitalPage({
    Key key,
    this.patientModel,
  }) : super(key: key);
  PatientModel patientModel;
  @override
  _SearchHospitalPageState createState() => _SearchHospitalPageState();
}

class _SearchHospitalPageState extends State<SearchHospitalPage> {
  PatientModel model;
  DBService dbService;
  DB dbHelper;
  String query = '';
  String minPrice = '';
  String maxPrice = '';
  String serviceName = 'Select Medical Service';
  List<HospitalModel> hospitals = [];
  Timer debouncer;

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    dbHelper = new DB();
    init();
    model = widget.patientModel;
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future init() async {
    final hospitals =
        await DB.searchHospital(query, minPrice, maxPrice, serviceName);

    setState(() => this.hospitals = hospitals);
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.patientName);
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Hospitals',
        ),
        backgroundColor: Colors.blue,
      ),
      body: _fetchData(),
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.patientName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Patient Profile',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PatientHome(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.solidHospital,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Hospitals',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SearchHospitalPage(
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Appointments',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewAppointmentsPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.edit_calendar,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Request',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => PatientRescheduleRequestPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 250,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _fetchData() {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                ),
              ),
            ),
            Column(
              children: [
                buildSearch(),
                // buildSearchServiceName(),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: SizedBox(
                    height: 60,
                    width: 360,
                    child: new DropdownButtonFormField<String>(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              width: 1.2,
                              color: Color.fromARGB(255, 204, 204, 204),
                            )),
                      ),
                      focusColor: Colors.blue,
                      isDense: true,
                      isExpanded: true,
                      hint: new Text("Medical Service"),
                      value: serviceName,
                      icon: const Icon(Icons.arrow_drop_down),
                      onChanged: (String newValue) {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        setState(() {
                          serviceName = newValue;
                          searchhospitalServiceName(serviceName);
                        });
                      },
                      items: <String>[
                        'Select Medical Service',
                        'Dental Services',
                        'Vaccinations',
                        'Mental Health',
                        'Internal Medicine',
                        'Pediatrics',
                        'Urology',
                        'Ophthalmology',
                        'Anesthesiology',
                        'Dermatology',
                        'Neurology',
                        'Surgery'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            style: TextStyle(
                              color: value == 'Select Medical Service'
                                  ? Colors.grey
                                  : Colors.black,
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    buildSearchMinPrice(),
                    buildSearchMaxPrice(),
                  ],
                ),
                Text('Number of Results ${hospitals.length}'),
                FutureBuilder<List<HospitalModel>>(
                  future:
                      DB.searchHospital(query, minPrice, maxPrice, serviceName),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<HospitalModel>> hospital) {
                    if (hospital.hasData) {
                      return Container(
                        height: 580,
                        width: 380,
                        child: ListView.builder(
                          itemCount: hospitals.length,
                          itemBuilder: (context, index) {
                            final hospital = hospitals[index];

                            return buildHospital(hospital);
                          },
                        ),
                      );
                    }

                    return CircularProgressIndicator();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Name/Location/Specialization',
        onChanged: searchhospital,
      );
  Widget buildSearchMinPrice() => SearchPriceWidget(
        text: minPrice,
        hintText: 'Min Price',
        onChanged: searchhospitalMinPrice,
      );

  Widget buildSearchMaxPrice() => SearchPriceWidget(
        text: maxPrice,
        hintText: 'Max Price',
        onChanged: searchhospitalMaxPrice,
      );

  DropdownMenuItem<String> buildMenuItem(String items) => DropdownMenuItem(
        value: items,
        child: Text(
          items,
          style: TextStyle(
            fontSize: 20,
          ),
        ),
      );

  Widget buildSearchServiceName() => SearchServiceWidget(
        text: serviceName,
        hintText: 'Medical Service',
        onChanged: searchhospitalServiceName,
      );

  Future searchhospital(String query) async => debounce(() async {
        final hospitals =
            await DB.searchHospital(query, minPrice, maxPrice, serviceName);

        if (!mounted) return;

        setState(() {
          this.query = query;
          this.hospitals = hospitals;
        });
      });

  Future searchhospitalMinPrice(String minPrice) async => debounce(() async {
        final hospitals =
            await DB.searchHospital(query, minPrice, maxPrice, serviceName);

        if (!mounted) return;

        setState(() {
          this.minPrice = minPrice;
          this.hospitals = hospitals;
        });
      });

  Future searchhospitalMaxPrice(String maxPrice) async => debounce(() async {
        final hospitals =
            await DB.searchHospital(query, minPrice, maxPrice, serviceName);

        if (!mounted) return;

        setState(() {
          this.maxPrice = maxPrice;
          this.hospitals = hospitals;
        });
      });

  Future searchhospitalServiceName(String serviceName) async =>
      debounce(() async {
        final hospitals =
            await DB.searchHospital(query, minPrice, maxPrice, serviceName);

        if (!mounted) return;

        setState(() {
          this.serviceName = serviceName;
          this.hospitals = hospitals;
        });
      });
  Widget buildHospital(HospitalModel hospital) => ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HospitalPage(
                model: hospital,
                patientModel: model,
              ),
            ),
          );
        },
        leading: hospital.hospitalPic.contains('https')
            ? Image.network(
                '${hospital.hospitalPic}',
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              )
            : hospital.hospitalPic != null
                ? Image.file(
                    File(hospital.hospitalPic),
                    fit: BoxFit.cover,
                    width: 50,
                    height: 50,
                  )
                : hospital.hospitalPic == null
                    ? Image.network(
                        'https://thumbs.dreamstime.com/b/hospital-building-modern-parking-lot-59693686.jpg',
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                      )
                    : Image.network(
                        'https://thumbs.dreamstime.com/b/hospital-building-modern-parking-lot-59693686.jpg',
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                      ),
        title: Text(hospital.hospitalName),
        subtitle: Text(hospital.address),
      );
}
