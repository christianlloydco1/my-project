// @dart=2.9
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/Comm/comHelper.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/Patients/hospital_page.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class ReceiptPage extends StatefulWidget {
  ReceiptPage(
      {Key key,
      this.patientModel,
      this.serviceModel,
      this.hospitalModel,
      this.appointmentModel,
      this.isEditMode = true})
      : super(key: key);
  PatientModel patientModel;
  HospitalModel hospitalModel;
  ServiceModel serviceModel;
  GeneralAppointmentModel appointmentModel;
  bool isEditMode;

  @override
  _ReceiptPageState createState() => _ReceiptPageState();
}

class _ReceiptPageState extends State<ReceiptPage> {
  PatientModel patientModel;
  HospitalModel hospitalModel;
  ServiceModel serviceModel;
  GeneralAppointmentModel appointmentModel;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    patientModel = new PatientModel();
    hospitalModel = new HospitalModel();
    serviceModel = new ServiceModel();
    appointmentModel = new GeneralAppointmentModel();

    if (widget.isEditMode) {
      patientModel = widget.patientModel;
      hospitalModel = widget.hospitalModel;
      serviceModel = widget.serviceModel;
      appointmentModel = widget.appointmentModel;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          automaticallyImplyLeading: true,
          title: Text("Receipt"),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.blue,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Form(
                key: globalFormKey,
                child: _formUI(),
              ),
              SizedBox(
                height: 20,
              ),
              btnSubmit(),
            ],
          ),
        ));
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          width: (MediaQuery.of(context).size.width) * .95,
          height: (MediaQuery.of(context).size.height) * .70,
          color: Colors.white,
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.check_circle, color: Colors.green),
                        Text(
                          'Appointment successfully set',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    Text(
                      '${patientModel.patientName.toUpperCase()}',
                      style: TextStyle(
                          color: Colors.blue,
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    const Divider(
                      height: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.grey,
                    ),
                    Text(
                      '${hospitalModel.hospitalName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    const Divider(
                      height: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.grey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '${serviceModel.serviceName}',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    const Divider(
                      height: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.grey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 150),
                          child: Text(
                            'Total',
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Icon(FontAwesomeIcons.pesoSign),
                        Text(
                          '${serviceModel.price}',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    const Divider(
                      height: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 20,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      '${appointmentModel.scheduleDate}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      '${appointmentModel.timeRange}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: TextButton(
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ViewAppointmentsPage(
                  patientModel: patientModel,
                ),
              ),
            );
          },
          child: Text(
            'Continue',
            style: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.bold,
                fontSize: 20),
          )),
    );
  }
}
