// @dart=2.9
import 'dart:io';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hospital_app/pages/Admin/hospitals_information.dart';
import 'package:hospital_app/pages/Hospitals/reschedule_request.dart';
import 'package:hospital_app/pages/Patients/hospitals_information.dart';
import 'package:hospital_app/pages/Patients/reschedule_request.dart';
import 'package:hospital_app/pages/Patients/search_hospitals.dart';
import 'package:hospital_app/pages/Patients/appointment_page.dart';
import 'package:hospital_app/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:hospital_app/pages/Admin/patients_information.dart';
import 'package:hospital_app/services/db_service.dart';
import 'package:hospital_app/utils/form_helper.dart';

class PatientHome extends StatefulWidget {
  PatientHome({Key key, this.patientModel, this.isEditMode = true})
      : super(key: key);
  PatientModel patientModel;
  bool isEditMode;

  @override
  _PatientHomeState createState() => _PatientHomeState();
}

class _PatientHomeState extends State<PatientHome> {
  PatientModel model;
  DBService dbService;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    dbService = new DBService();
    model = new PatientModel();

    if (widget.isEditMode) {
      model = widget.patientModel;
    }
  }

  String getInitials(String name) => name.isNotEmpty
      ? name.trim().split(' ').map((l) => l[0]).take(2).join()
      : '';

  @override
  Widget build(BuildContext context) {
    String name = getInitials(model.patientName);
    // ignore: unnecessary_new
    return new Scaffold(
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 114, 187, 247),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 210,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.blue.shade800,
                      child: Text(
                        '$name',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                        ),
                      ),
                      radius: 40,
                    ),
                    Text(
                      '${model.patientName}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${model.email}',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.white,
              ),
              tileColor: Colors.blue,
              title: const Text(
                'Patient Profile',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PatientHome(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.solidHospital,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Hospitals',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => SearchHospitalPage(
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.calendarCheck,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Appointments',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => ViewAppointmentsPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.edit_calendar,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Reschedule Request',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => PatientRescheduleRequestPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 250,
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
              ),
              tileColor: Color.fromARGB(255, 114, 187, 247),
              title: const Text(
                'Logout',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(
                      isEditMode: true,
                      patientModel: model,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: true,
        title: Text("Patient Profile"),
      ),
      body: new Form(
        key: globalFormKey,
        child: _formUI(),
      ),
    );
  }

  Widget _formUI() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                model.patientPic != null
                    ? FormHelper.picPicker(
                        model.patientPic,
                        (file) => {
                          setState(
                            () {
                              model.patientPic = file.path;
                            },
                          )
                        },
                      )
                    : FormHelper.picPicker2(
                        model.patientPic,
                        (file) => {
                          setState(
                            () {
                              model.patientPic = file.path;
                            },
                          )
                        },
                      ),
                SizedBox(
                  height: 20,
                ),

                FormHelper.textInputEmail(
                  context,
                  model.email,
                  (value) => {
                    this.model.email = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter Patient Name.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Email should be in this format email@gmail.com';
                    }
                    return null;
                  },
                ),

                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputPassword(
                  context,
                  model.password,
                  (value) => {
                    this.model.password = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter password.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Password should have uppercase, lowercase, numeric and special character.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                FormHelper.textInputName(
                  context,
                  model.patientName,
                  (value) => {
                    this.model.patientName = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Patient Name.';
                    } else if (value.toString().length < 8) {
                      return 'Name should be at least 8 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputAddress(
                  context,
                  model.address,
                  (value) => {
                    this.model.address = value,
                  },
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Address.';
                    } else if (value.toString().length < 10) {
                      return 'Address should be at least 10 characters.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputBday(
                  context,
                  model.bDay,
                  (value) => {
                    this.model.bDay = value,
                  },
                  onValidate: (value) {
                    String pattern =
                        r'(^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$)';

                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter birthday.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Birthday should be in this format MM/DD/YYYY.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                // FormHelper.fieldLabel("Address"),
                // FormHelper.textInput(
                //     context,
                //     model.address,
                //     (value) => {
                //           this.model.address = value,
                //         },
                //     isTextArea: true, onValidate: (value) {
                //   return null;
                // }),
                FormHelper.textInputGender(
                  context,
                  model.gender,
                  (value) => {
                    this.model.gender = value,
                  },
                  onValidate: (value) {
                    String pattern = r'(^(?:m|M|male|Male|f|F|female|Female)$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter gender.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Gender should be Male or Female.';
                    }

                    return null;
                  },
                ),

                SizedBox(
                  height: 15,
                ),

                FormHelper.textInputContact(
                  context,
                  model.contact,
                  (value) => {
                    this.model.contact = value,
                  },
                  isNumberInput: true,
                  onValidate: (value) {
                    String pattern = r'(^(09|\+639)\d{9}$)';
                    RegExp regExp = new RegExp(pattern);
                    if (value.toString().isEmpty) {
                      return 'Please enter contact number.';
                    } else if (!regExp.hasMatch(value)) {
                      return 'Contact number should be in this format 09#########.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),

                SizedBox(
                  height: 15,
                ),

                SizedBox(
                  height: 30,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          onPrimary: Colors.white,
        ),
        onPressed: () {
          if (validateAndSave()) {
            print(model.toMap());

            dbService.updatePatient(model).then((value) {
              FormHelper.showMessage(
                context,
                "Update Patient Record",
                "Patient record updated successfully",
                "Ok",
                () {
                  Navigator.pop(context, true);
                },
              );
            });
          }
        },
        child: Text('Change Profile Information'),
      ),
    );
  }
}
