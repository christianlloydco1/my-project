// @dart=2.9
import 'dart:io';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:hospital_app/models/data_model.dart';
import 'package:image_picker/image_picker.dart';

class FormHelper {
  String serviceName;
  List<String> items = ['Item1', 'Item2'];
  static Widget textInputEmail(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.email),
        hintText: 'chris@gmail.com',
        labelText: 'Email',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputServiceName(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.transparent),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(FontAwesomeIcons.userDoctor),
        hintText: 'Dental Care',
        labelText: 'Medical Service',
        fillColor: Color.fromARGB(255, 238, 238, 238),
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputServiceName2(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      enabled: false,
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.transparent),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(FontAwesomeIcons.userDoctor),
        hintText: 'Dental Care',
        labelText: 'Medical Service',
        fillColor: Color.fromARGB(255, 238, 238, 238),
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputServicePrice(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.transparent),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.price_change),
        hintText: '500',
        labelText: 'Price',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputServicePrice2(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      enabled: false,
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.transparent),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.price_change),
        hintText: '500',
        labelText: 'Price',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputPassword(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.lock),
        hintText: 'Password',
        labelText: 'Password',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputName(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.person),
        hintText: 'Juan Dela Cruz',
        labelText: 'Name',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputAddress(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.lock),
        hintText: 'E.R. Ph 2 Blk 48 Lot 9, San Isidro, Rodriguez, Rizal',
        labelText: 'Address',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputBday(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(FontAwesomeIcons.cakeCandles),
        hintText: 'MM/DD/YYYY',
        labelText: 'Birthday',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  // DateTimePicker(
  //                     controller: _birthdayController,
  //                     initialDate: new DateTime(2000, 1, 1),
  //                     firstDate: DateTime(1900),
  //                     lastDate: DateTime(2022),
  //                     onChanged: bloc.changeBirthday,
  //                     decoration: InputDecoration(
  //                       hintText: 'MM/DD/YYYY',
  //                       labelText: 'Birthday',
  //                       errorText: snapshot.error?.toString(),
  //                     ),
  //                     validator: (val) {
  //                       print(val);
  //                       return null;
  //                     },
  //                     onSaved: (val) => print(val),
  //                   );
  static Widget textInputContact(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(Icons.phone),
        hintText: '095456987456',
        labelText: 'Contact Number',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  static Widget textInputGender(
    BuildContext context,
    Object initialValue,
    Function onChanged, {
    bool isNumberInput = false,
    Function onValidate,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        prefixIcon: Icon(FontAwesomeIcons.marsAndVenus),
        hintText: 'Male/Female',
        labelText: 'Gender',
        fillColor: Colors.grey[200],
        filled: true,
      ),
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        return onValidate(value);
      },
    );
  }

  Widget selectDropdown(
    BuildContext context,
    Object initialValue,
    dynamic data,
    Function onChanged, {
    Function onValidate,
  }) {
    return Column(
      children: [
        Container(
          height: 90,
          padding: EdgeInsets.only(top: 5),
          child: SizedBox(
            child: new DropdownButtonFormField<String>(
              decoration: InputDecoration(
                focusColor: Colors.blue,
                suffixIconColor: Colors.blue,
                fillColor: Colors.blue,
                hintText: 'Select Medical Service',
                labelText: 'Medical Service',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              isExpanded: true,
              hint: new Text("Select Medical Service"),
              value: initialValue != null ? initialValue.toString() : null,
              isDense: true,
              onChanged: (String newValue) {
                FocusScope.of(context).requestFocus(new FocusNode());
                onChanged(newValue);

                print(newValue);
              },
              validator: (value) {
                return onValidate(value);
              },
              items: data.map<DropdownMenuItem<String>>(
                (ServiceModel data) {
                  return DropdownMenuItem<String>(
                    value: data.serviceId.toString(),
                    child: new Text(
                      data.serviceName,
                      style: new TextStyle(color: Colors.black),
                    ),
                  );
                },
              ).toList(),
            ),
          ),
        ),
        Text(serviceName),
        Container(
          height: 90,
          padding: EdgeInsets.only(top: 5),
          child: SizedBox(
            child: new DropdownButtonFormField<String>(
              decoration: InputDecoration(
                hintText: 'Price',
                labelText: 'Price',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              isExpanded: true,
              hint: new Text("Price"),
              value: initialValue != null ? initialValue.toString() : null,
              isDense: true,
              onChanged: null,
              validator: (value) {
                return onValidate(value);
              },
              items: data.map<DropdownMenuItem<String>>(
                (ServiceModel data) {
                  return DropdownMenuItem<String>(
                    value: data.serviceId.toString(),
                    child: new Text(
                      data.price,
                      style: new TextStyle(color: Colors.black),
                    ),
                  );
                },
              ).toList(),
            ),
          ),
        ),
      ],
    );
  }

  static Widget fieldLabel(String labelName) {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
      child: Text(
        labelName,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15.0,
        ),
      ),
    );
  }

  static Widget picPicker(String fileName, Function onFilePicked) {
    Future<PickedFile> _imageFile;
    ImagePicker _picker = new ImagePicker();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.image, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.gallery);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                icon: Icon(Icons.camera, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.camera);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
          ],
        ),
        fileName.contains('https')
            ? Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.grey[500],
                  ),
                ),
                width: 200,
                height: 200,
                child: Image.network(
                  '$fileName',
                  fit: BoxFit.fill,
                ),
              )
            : fileName != null
                ? Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(
                        color: Colors.grey[500],
                      ),
                    ),
                    width: 200,
                    height: 200,
                    child: Image.file(
                      File(fileName),
                      fit: BoxFit.fill,
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(
                        color: Colors.grey[500],
                      ),
                    ),
                    padding: EdgeInsets.only(left: 5, top: 70),
                    width: 200,
                    height: 200,
                    child: Column(
                      children: [
                        Text(
                          'Select Photo',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                              color: Colors.white),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Icon(
                          Icons.add_a_photo,
                          color: Colors.white,
                        )
                      ],
                    ),
                  )
      ],
    );
  }

  static Widget picPicker2(String fileName, Function onFilePicked) {
    Future<PickedFile> _imageFile;
    ImagePicker _picker = new ImagePicker();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.image, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.gallery);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
            SizedBox(
              height: 35.0,
              width: 35.0,
              child: new IconButton(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                icon: Icon(Icons.camera, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.camera);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
          ],
        ),
        fileName != null
            ? Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.grey[500],
                  ),
                ),
                width: 200,
                height: 200,
                child: Image.file(
                  File(fileName),
                  fit: BoxFit.fill,
                ),
              )
            : Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  border: Border.all(
                    color: Colors.grey[500],
                  ),
                ),
                padding: EdgeInsets.only(left: 5, top: 70),
                width: 200,
                height: 200,
                child: Column(
                  children: [
                    Text(
                      'Select Photo',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_a_photo,
                      color: Colors.white,
                    )
                  ],
                ),
              )
      ],
    );
  }

  // static Widget picPicker2(String fileName, Function onFilePicked) {
  //   Future<PickedFile> _imageFile;
  //   ImagePicker _picker = new ImagePicker();

  //   return Column(
  //     children: [
  //       fileName != null
  //           ? Container(
  //               decoration: BoxDecoration(
  //                 color: Colors.blue,
  //                 border: Border.all(
  //                   color: Colors.grey[500],
  //                 ),
  //               ),
  //               width: 200,
  //               height: 200,
  //               child: Image.file(
  //                 File(fileName),
  //                 fit: BoxFit.fill,
  //               ),
  //             )
  //           : Container(
  //               decoration: BoxDecoration(
  //                 color: Colors.blue,
  //                 border: Border.all(
  //                   color: Colors.grey[500],
  //                 ),
  //               ),
  //               padding: EdgeInsets.only(left: 5, top: 70),
  //               width: 200,
  //               height: 200,
  //               child: Column(
  //                 children: [
  //                   Text(
  //                     'Select Photo',
  //                     style: TextStyle(
  //                         fontWeight: FontWeight.bold,
  //                         fontSize: 25,
  //                         color: Colors.white),
  //                   ),
  //                   SizedBox(
  //                     height: 10,
  //                   ),
  //                   Icon(
  //                     Icons.add_a_photo,
  //                     color: Colors.white,
  //                   )
  //                 ],
  //               ),
  //             )
  //     ],
  //   );
  // }

  static void showMessage(BuildContext context, String title, String message,
      String buttonText, Function onPressed,
      {bool isConfirmationDialog = false,
      String buttonText2 = "",
      Function onPressed2}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 600,
          width: 400,
          child: AlertDialog(
            insetPadding: EdgeInsets.all(20),
            title: new Text(title),
            content: new Text(message),
            actions: [
              new FlatButton(
                onPressed: () {
                  return onPressed();
                },
                child: new Text(buttonText),
              ),
              Visibility(
                visible: isConfirmationDialog,
                child: new FlatButton(
                  onPressed: () {
                    return onPressed2();
                  },
                  child: new Text(buttonText2),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
