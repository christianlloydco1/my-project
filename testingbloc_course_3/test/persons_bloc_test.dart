import 'package:flutter_test/flutter_test.dart'; //test structure is not dart test file but  flutter test file
import 'package:bloc_test/bloc_test.dart';
import 'package:testingbloc_course_2/bloc/bloc_actions.dart';
import 'package:testingbloc_course_2/bloc/person.dart';
import 'package:testingbloc_course_2/bloc/persons_bloc.dart';

//add mock iterable
final mockedPersons1 = [
  const Person(
    age: 20,
    name: 'Foo',
  ),
  const Person(
    age: 30,
    name: 'Bar',
  ),
];

final mockedPersons2 = [
  const Person(
    age: 20,
    name: 'Foo',
  ),
  const Person(
    age: 30,
    name: 'Bar',
  ),
];
//function that always get iterable for mockedPersons1
Future<Iterable<Person>> mockGetPersons1(String _) =>
    Future.value(mockedPersons1);
//function that always get iterable for mockedPersons2
Future<Iterable<Person>> mockGetPersons2(String _) =>
    Future.value(mockedPersons2);

void main() {
  //group is used when the usnit tests that you are doing are related to each other
  group('Testing bloc', () {
    late PersonsBloc bloc;
    setUp(() {
      // any test that we write in this group will have access to fresh copy of PersonsBloc, every test that we run there will be new instance of personsBloc
      bloc = PersonsBloc();
    });
    //allow us to test our bloc
    blocTest<PersonsBloc, FetchResult?>(
      'Test initial state',
      build: () => bloc,
      verify: (bloc) => bloc.state == null,
      //verify if the initial state of the bloc is null
    );

    //fetch mock data (persons1) and compare it with fetchResult
    blocTest<PersonsBloc, FetchResult?>(
      'Mock retrieving persons from first iterable',
      build: () => bloc,
      act: (bloc) {
        bloc.add(
          const LoadPersonsAction(
            url: 'dummy_url',
            loader: mockGetPersons1,
          ),
        );

        bloc.add(
          const LoadPersonsAction(
            url: 'dummy_url',
            loader: mockGetPersons1,
          ),
        );
      },
      expect: () => [
        FetchResult(
          persons: mockedPersons1,
          isRetrievedFromCache: false,
        ),
        FetchResult(
          persons: mockedPersons1,
          isRetrievedFromCache: true,
        ),
      ],
    );

    //fetch mock data (persons2) and compare it with fetchResult
    blocTest<PersonsBloc, FetchResult?>(
      'Mock retrieving persons from second iterable',
      build: () => bloc,
      act: (bloc) {
        bloc.add(
          const LoadPersonsAction(
            url: 'dummy_url_2',
            loader: mockGetPersons2,
          ),
        );

        bloc.add(
          const LoadPersonsAction(
            url: 'dummy_url_2',
            loader: mockGetPersons2,
          ),
        );
      },
      expect: () => [
        FetchResult(
          persons: mockedPersons2,
          isRetrievedFromCache: false,
        ),
        FetchResult(
          persons: mockedPersons2,
          isRetrievedFromCache: true,
        ),
      ],
    );
  });
}
