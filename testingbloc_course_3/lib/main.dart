import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testingbloc_course_2/bloc/bloc_actions.dart';
import 'dart:async';
import 'dart:developer' as devtools show log;
import 'bloc/person.dart';
import 'bloc/persons_bloc.dart';

extension Log on Object {
  void log() => devtools.log(toString());
}

void main() {
  runApp(
    MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        //adding provider to give home page access to the bloc
        create: (_) => PersonsBloc(),
        child: const HomePage(),
      ),
    ),
  );
}

//model class

//created to put subscript on iterable
extension Subscript<T> on Iterable<T> {
  T? operator [](int index) => length > index ? elementAt(index) : null;
}

//downlaod and parse json .then syntax
Future<Iterable<Person>> getPersons(String url) => HttpClient()
    .getUrl(Uri.parse(url)) //makes request
    .then((req) => req.close()) //close the request
    .then((resp) =>
        resp.transform(utf8.decoder).join()) //transform the request into string
    .then((str) => json.decode(str) as List<dynamic>) //Make the string a list
    .then((list) => list.map(
        (e) => Person.fromJson(e))); //List becomes iterable of Person class

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //add row with two buttons and actions to column
      body: Column(
        children: [
          Row(
            children: [
              TextButton(
                onPressed: () {
                  context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                          url: persons1url,
                          loader: getPersons,
                        ),
                      ); //.read like of function goes up in the widget tree and find the provider that gives access to the bloc.
                },
                child: const Text('Load json#1'),
              ),
              TextButton(
                onPressed: () {
                  context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                          url: persons2url,
                          loader: getPersons,
                        ),
                      );
                },
                child: const Text('Load json#2'),
              ),
            ],
          ),
          //use blocBuilder to display content on the screen
          BlocBuilder<PersonsBloc, FetchResult?>(
            buildWhen: (previousResult, currentResult) {
              return previousResult?.persons != currentResult?.persons;
            },
            builder: ((context, fetchResult) {
              fetchResult?.log();
              final persons = fetchResult?.persons;
              if (persons == null) {
                return const SizedBox();
              }
              return Expanded(
                child: ListView.builder(
                  itemCount: persons.length,
                  itemBuilder: (context, index) {
                    final person = persons[index]!;
                    return ListTile(
                      title: Text('Name: ${person.name} \nAge: ${person.age}'),
                    );
                  },
                ),
              );
            }),
          )
        ],
      ),
      appBar: AppBar(
        title: const Text('Home Page'),
      ),
    );
  }
}
