import 'package:firebase_auth/firebase_auth.dart' show FirebaseAuthException;
import 'package:flutter/foundation.dart' show immutable;

//map created for mapping error thrown by firebase
const Map<String, AuthError> authErrorMapping = {
  'user-not-found': AuthErrorUserNotFound(),
  'weak-password': AuthErrorWeakPassword(),
  'auth/invalid-email': AuthErrorInvalidEmail(),
  'operation-not-allowed': AuthErrorOperationNotAllowed(),
  'email-already-in-use': AuthErrorEmailAlreadyInUse(),
  'requires-recent-login': AuthErrorRequiresRecentLogin(),
  'no-current-user': AuthErrorNoCurrentUser(),
};

//define base AuthError class
@immutable
abstract class AuthError {
  final String dialogueTitle;
  final String dialogueText;

  const AuthError({
    required this.dialogueTitle,
    required this.dialogueText,
  });
//get exception error thrown by firebase
  factory AuthError.from(FirebaseAuthException exception) =>
      authErrorMapping[exception.code.toLowerCase().trim()] ??
      const AuthErrorUnkown();
}

@immutable
class AuthErrorUnkown extends AuthError {
  const AuthErrorUnkown()
      : super(
          dialogueTitle: 'Authentication Error',
          dialogueText: 'Unkown authentication error',
        );
}

//class clustering
//auth/no-current-user
@immutable
class AuthErrorNoCurrentUser extends AuthError {
  const AuthErrorNoCurrentUser()
      : super(
          dialogueTitle: 'No current user!',
          dialogueText: 'No current user with this information was found!',
        );
}

@immutable
//auth/requires-recent-login
class AuthErrorRequiresRecentLogin extends AuthError {
  const AuthErrorRequiresRecentLogin()
      : super(
          dialogueTitle: 'Requires recent login',
          dialogueText:
              'You need to log out and log back in again in order to perform this operation',
        );
}

//auth/operation-not-allowed
//this only happends when emal and password authentication or other authentication used by user to log in is not enabled in firebase
@immutable
class AuthErrorOperationNotAllowed extends AuthError {
  const AuthErrorOperationNotAllowed()
      : super(
          dialogueTitle: 'Operation not allowed',
          dialogueText: 'You cannot register using this method at this moment!',
        );
}

//auth/user-not-found
@immutable
class AuthErrorUserNotFound extends AuthError {
  const AuthErrorUserNotFound()
      : super(
          dialogueTitle: 'User not found',
          dialogueText: 'The given user was not found on the server!',
        );
}

//auth/weak-password
@immutable
class AuthErrorWeakPassword extends AuthError {
  const AuthErrorWeakPassword()
      : super(
          dialogueTitle: 'Weak password',
          dialogueText:
              'Please choose a stronger password consisting of more characters!',
        );
}

//auth/invalid-email
@immutable
class AuthErrorInvalidEmail extends AuthError {
  const AuthErrorInvalidEmail()
      : super(
          dialogueTitle: 'Invalid email',
          dialogueText: 'Please double check your email and try again',
        );
}

//email-already-in-use
@immutable
class AuthErrorEmailAlreadyInUse extends AuthError {
  const AuthErrorEmailAlreadyInUse()
      : super(
          dialogueTitle: 'Email already in use',
          dialogueText: 'Please choose another email to register with!',
        );
}
