import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:testingbloc_course_8/firebase_options.dart';
import 'package:testingbloc_course_8/views/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(
    const App(),
  );
}
