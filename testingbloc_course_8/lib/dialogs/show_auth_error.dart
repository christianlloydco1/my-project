import 'package:flutter/material.dart' show BuildContext;
import 'package:testingbloc_course_8/auth/auth_error.dart';
import 'package:testingbloc_course_8/dialogs/generic_dialog.dart';

Future<void> showAuthError({
  required AuthError authError,
  required BuildContext context,
}) {
  return showGenericDialog<void>(
    context: context,
    title: authError.dialogueTitle,
    content: authError.dialogueText,
    optionsBuilder: () => {
      'OK': true,
    },
  );
}
