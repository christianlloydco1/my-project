import 'package:flutter/foundation.dart' show immutable;

@immutable
abstract class AppEvent {
  const AppEvent();
}

//event for uploading image
@immutable
class AppEventUploadImage implements AppEvent {
  final String filePathToUpload;

  const AppEventUploadImage({
    required this.filePathToUpload,
  });
}

//event for uploading image
@immutable
class AppEventDeleteAccount implements AppEvent {
  const AppEventDeleteAccount();
}

//event for logging in the user
@immutable
class AppEventLogIn implements AppEvent {
  final String email;
  final String password;

  const AppEventLogIn({
    required this.email,
    required this.password,
  });
}

//event for registration
@immutable
class AppEventRegister implements AppEvent {
  final String email;
  final String password;

  const AppEventRegister({
    required this.email,
    required this.password,
  });
}

//event for logging out the user
@immutable
class AppEventLogOut implements AppEvent {
  const AppEventLogOut();
}

//event for initializing the state to keep user logged in or out
@immutable
class AppEventInitialize implements AppEvent {
  const AppEventInitialize();
}

//event for displaying registration view
@immutable
class AppEventGoToRegistration implements AppEvent {
  const AppEventGoToRegistration();
}

//event for displaying login view
@immutable
class AppEventGoToLogin implements AppEvent {
  const AppEventGoToLogin();
}
