//method that allows us to upload image
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';

Future<bool> uploadImage({
  //takes file and user id
  required File file,
  required String userId,
}) =>
    FirebaseStorage.instance
        .ref(userId) //choose a folder same with the user id
        .child(const Uuid().v4()) //create file
        .putFile(file) //pute our file into the created file
        .then((_) => true) //return true if there is no problem
        .catchError((_) => false);// otherwise return false
