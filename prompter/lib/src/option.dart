// @dart=2.9
class Option {
  String label;
  dynamic value;

  Option(this.label, this.value);
}
