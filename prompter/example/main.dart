// @dart=2.9
import 'package:prompter_cloc/prompter_cloc.dart';

void main() {
  final options = [
    new Option('I want red', '#f00'),
    new Option('I want blue', '00f')
  ];

  final prompter = new Prompter();
  String colorCode = prompter.askMultiple('Select a color', options);
  bool answer = prompter.askBinary('Do you like dart?');

  print(colorCode);
  print(answer);
}
